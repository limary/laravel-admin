<?php
namespace Sinta\Laravel\Admin\Facades;

use Illuminate\Support\Facades\Facade;


/**
 * 门面
 *
 * Class Admin
 * @package Sinta\Laravel\Admin\Facades
 */
class Admin extends Facade
{
    protected static function getFacadeAccessor()
    {
        return \Sinta\Laravel\Admin\Admin::class;
    }
}