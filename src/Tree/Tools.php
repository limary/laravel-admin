<?php
namespace Sinta\Laravel\Admin\Tree;

use Illuminate\Support\Collection;
use Illuminate\Contracts\Support\Htmlable;
use Illuminate\Contracts\Support\Renderable;
use Sinta\Laravel\Admin\Tree;

/**
 * 树工具
 *
 * Class Tools
 * @package Sinta\Hippo\Tree
 */
class Tools implements Renderable
{
    protected $tree;

    protected $tools;

    public function __construct(Tree $tree)
    {
        $this->tree = $tree;
        $this->tools = new Collection();
    }

    public function add($tool)
    {
        $this->tools->push($tool);

        return $this;
    }


    /**
     * 渲染
     *
     * @return string
     */
    public function render()
    {
        return $this->tools->map(function ($tool) {
            if ($tool instanceof Renderable) {
                return $tool->render();
            }

            if ($tool instanceof Htmlable) {
                return $tool->toHtml();
            }

            return (string) $tool;
        })->implode(' ');
    }
}