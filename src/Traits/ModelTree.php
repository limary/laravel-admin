<?php
namespace Sinta\Laravel\Admin\Traits;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request;
use Sinta\Laravel\Admin\Tree\Tree;


trait ModelTree
{
    protected static $branchOrder = [];

    protected $parentColumn = 'parent_id';

    protected $titleColumn = 'title';

    protected $orderColumn = 'order';

    protected $queryCallback;

    public function children()
    {
        return $this->hasMany(static::class, $this->parentColumn);
    }

    public function parent()
    {
        return $this->belongsTo(static::class, $this->parentColumn);
    }

    public function getParentColumn()
    {
        return $this->parentColumn;
    }

    public function setParentColumn($column)
    {
        $this->parentColumn = $column;
    }

    public function getTitleColumn()
    {
        return $this->titleColumn;
    }



    public function setTitleColumn($column)
    {
        $this->titleColumn = $column;
    }

    public function getOrderColumn()
    {
        return $this->orderColumn;
    }

    public function setOrderColumn($column)
    {
        $this->orderColumn = $column;
    }

    public function withQuery(\Closure $query = null)
    {
        $this->queryCallback = $query;

        return $this;
    }


    public function toTree()
    {
        return $this->buildNestedArray();
    }

    protected function buildNestedArray(array $nodes = [], $parentId = 0)
    {
        $branch = [];

        if (empty($nodes)) {
            $nodes = $this->allNodes();
        }

        foreach ($nodes as $node) {
            if ($node[$this->parentColumn] == $parentId) {
                $children = $this->buildNestedArray($nodes, $node[$this->getKeyName()]);

                if ($children) {
                    $node['children'] = $children;
                }

                $branch[] = $node;
            }
        }

        return $branch;
    }


    public function allNodes()
    {
        $orderColumn = DB::getQueryGrammar()->wrap($this->orderColumn);
        $byOrder = $orderColumn.' = 0,'.$orderColumn;

        $self = new static();

        if ($this->queryCallback instanceof \Closure) {
            $self = call_user_func($this->queryCallback, $self);
        }

        return $self->orderByRaw($byOrder)->get()->toArray();
    }


    /**
     * 设置排序
     *
     * @param array $order
     */
    protected static function setBranchOrder(array $order)
    {
        static::$branchOrder = array_flip(array_flatten($order));

        static::$branchOrder = array_map(function ($item) {
            return ++$item;
        }, static::$branchOrder);
    }

    /**
     * 保存排序
     *
     * @param array $tree
     * @param int $parentId
     */
    public static function saveOrder($tree = [], $parentId = 0)
    {
        if (empty(static::$branchOrder)) {
            static::setBranchOrder($tree);
        }

        foreach ($tree as $branch) {
            $node = static::find($branch['id']);

            $node->{$node->getParentColumn()} = $parentId;
            $node->{$node->getOrderColumn()} = static::$branchOrder[$branch['id']];
            $node->save();

            if (isset($branch['children'])) {
                static::saveOrder($branch['children'], $branch['id']);
            }
        }
    }


    public static function selectOptions()
    {
        $options = (new static())->buildSelectOptions();

        return collect($options)->prepend('Root', 0)->all();
    }


    protected function buildSelectOptions(array $nodes = [], $parentId = 0, $prefix = '')
    {
        $prefix = $prefix ?: str_repeat('&nbsp;', 6);

        $options = [];

        if (empty($nodes)) {
            $nodes = $this->allNodes();
        }

        foreach ($nodes as $node) {
            $node[$this->titleColumn] = $prefix.'&nbsp;'.$node[$this->titleColumn];
            if ($node[$this->parentColumn] == $parentId) {
                $children = $this->buildSelectOptions($nodes, $node[$this->getKeyName()], $prefix.$prefix);

                $options[$node[$this->getKeyName()]] = $node[$this->titleColumn];

                if ($children) {
                    $options += $children;
                }
            }
        }

        return $options;
    }


    /**
     * 删除
     *
     * @return mixed
     */
    public function delete()
    {
        $this->where($this->parentColumn, $this->getKey())->delete();

        return parent::delete();
    }


    protected static function boot()
    {
        parent::boot();

        static::saving(function(Model $branch){
            $parentColumn = $branch->getParentColumn();

            if (Request::has($parentColumn) && Request::input($parentColumn) == $branch->getKey()) {
                throw new \Exception(trans('admin.parent_select_error'));
            }

            if (Request::has('_order')) {
                $order = Request::input('_order');

                Request::offsetUnset('_order');

                static::tree()->saveOrder($order);

                return false;
            }

            return $branch;
        });
    }

}