<?php

namespace Sinta\Laravel\Admin\Traits;

use Sinta\Laravel\Admin\Grid;
use Sinta\Laravel\Admin\Form;
use Sinta\Laravel\Admin\Tree;


trait AdminBuilder
{

    public static function grid(\Closure $callback)
    {
        return new Grid(new static(), $callback);
    }


    public static function form(\Closure $callback)
    {
        Form::registerBuiltinFields();

        return new Form(new static(), $callback);
    }

    public static function tree(\Closure $callback = null)
    {
        return new Tree(new static(), $callback);
    }
}