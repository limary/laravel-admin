<?php
namespace Sinta\Laravel\Admin;

use Closure;
use Illuminate\Database\Eloquent\Model as EloquentModel;
use Illuminate\Support\Facades\Auth;
use InvalidArgumentException;
use Sinta\Laravel\Admin\Widgets\Navbar;

/**
 * 聚合类
 *
 * Class Admin
 * @package Sinta\Laravel\Admin
 */
class Admin
{
    //导航
    protected $navbar;


    public static $script = [];

    public static $css = [];

    public static $js = [];

    //扩展
    public static $extensions = [];



    public function grid($model, Closure $callable)
    {
        return new Grid($this->getModel($model), $callable);
    }


    public function controllerNamespace()
    {
        $directory = config('admin.directory');

        return app()->getNamespace().ucfirst(basename($directory)).'\\Controllers';
    }




    public static function url($url)
    {
        $prefix = (string) config('admin.prefix');

        if (empty($prefix) || $prefix == '/') {
            return '/'.trim($url, '/');
        }

        return "/$prefix/".trim($url, '/');
    }

    public function form($model, Closure $callable)
    {
        return new Form($this->getModel($model), $callable);
    }


    public function tree($model, Closure $callable = null)
    {
        return new Tree($this->getModel($model));
    }



    public function content(Closure $callable = null)
    {
        return new Content($callable);
    }


    public function getModel($model)
    {
        if ($model instanceof EloquentModel) {
            return $model;
        }
        if (is_string($model) && class_exists($model)) {
            return $this->getModel(new $model());
        }
        throw new InvalidArgumentException("$model is not a valid model");
    }


    public static function css($css = null)
    {
        if (!is_null($css)) {
            self::$css = array_merge(self::$css, (array) $css);

            return;
        }

        $css = array_get(Form::collectFieldAssets(), 'css', []);

        static::$css = array_merge(static::$css, $css);

        return view('admin::partials.css', ['css' => array_unique(static::$css)]);
    }


    public static function js($js = null)
    {
        if (!is_null($js)) {
            self::$js = array_merge(self::$js, (array) $js);

            return;
        }

        $js = array_get(Form::collectFieldAssets(), 'js', []);

        static::$js = array_merge(static::$js, $js);

        return view('admin::partials.js', ['js' => array_unique(static::$js)]);
    }


    public static function script($script = '')
    {
        if (!empty($script)) {
            self::$script = array_merge(self::$script, (array) $script);

            return;
        }

        return view('admin::partials.script', ['script' => array_unique(self::$script)]);
    }

    public function menu()
    {
        return (new Menu())->toTree();
    }

    public function title()
    {
        return config('admin.title');
    }

    public function user()
    {
        return Auth::guard('admin')->user();
    }


    /**
     * navbar设置
     *
     * @param Closure|null $builder
     * @return Navbar
     */
    public function navbar(Closure $builder = null)
    {
        if(is_null($builder)){
            return $this->getNavbar();
        }
        call_user_func($builder, $this->getNavbar());
    }






    /**
     * 获取Navbar
     *
     * @return Navbar
     */
    public function getNavbar()
    {
        if(is_null($this->navbar)){
            $this->navbar = new Navbar();
        }
        return $this->navbar;
    }


    /**
     * 添加扩展
     *
     * @param $name
     * @param $class
     */
    public static function extend($name, $class)
    {
        static::$extensions[$name] = $class;
    }


}