<?php
namespace Sinta\Laravel\Admin\Exception;

use Illuminate\Support\MessageBag;
use Illuminate\Support\ViewErrorBag;

class Handler
{
    public static function renderException(\Exception $exception)
    {
        $error = new MessageBag([
            'type'    => get_class($exception),
            'message' => $exception->getMessage(),
            'file'    => $exception->getFile(),
            'line'    => $exception->getLine(),
        ]);
        $errors = new ViewErrorBag();
        $errors->put('exception', $error);
        return view('admin::partials.exception', compact('errors'))->render();
    }

    public static function error($title = '', $message = '')
    {
        $error = new MessageBag(compact('title', 'message'));
        return session()->flash('error', $error);
    }
}