<?php
namespace Sinta\Laravel\Admin;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;

class Form
{

    /**
     *
     * @var Model
     */
    protected $model;


    /**
     * 验证器
     *
     * @var \Illuminate\Validation\Validator
     */
    protected $validator;


    /**
     * 表单创建器
     *
     * @var Builder
     */
    protected $builder;

    /**
     * @var Closure
     */
    protected $submitted;

    /**
     * 保存时回调
     *
     * @var Closure
     */
    protected $saving;

    /**
     * 保存后回调
     *
     * @var Closure
     */
    protected $saved;


    /**
     * 当前模型更新的数据
     *
     * @var array
     */
    protected $updates = [];


    /**
     * 关系相关数据
     *
     * @var array
     */
    protected $relations = [];


    protected $inputs = [];


    /**
     * 有效的字段
     *
     * @var array
     */
    public static $availableFields = [];


    /**
     * 不保存数据
     *
     * @var array
     */
    protected $ignored = [];


    protected static $collectedAssets = [];

    protected $tab = null;

    const REMOVE_FLAG_NAME = '_remove_';


    public function __construct($model,Closure $callback)
    {
        $this->model = $model;
        $this->builder = new Builder($this);
        $callback($this);
    }

    /**
     * 添加字段
     *
     * @param Field $field
     * @return $this
     */
    public function pushField(Field $field)
    {
        $field->setForm($this);
        $this->builder->fields()->push($field);
        return $this;
    }


    /**
     * 当前模型
     *
     * @return Model
     */
    public function model()
    {
        return $this->model;
    }

    /**
     * 创建器
     *
     * @return Builder
     */
    public function builder()
    {
        return $this->builder;
    }


    /**
     * 编辑模块
     *
     * @param $id
     * @return $this
     */
    public function edit($id)
    {
        $this->builder->setMode(Builder::MODE_EDIT);
        $this->builder->setResourceId($id);

        $this->setFieldValue($id);
        return $this;
    }

    /**
     * 查看
     *
     * @param $id
     * @return $this
     */
    public function view($id)
    {
        $this->builder->setMode(Builder::MODE_VIEW);
        $this->builder->setResourceId($id);

        $this->setFieldValue($id);

        return $this;
    }

    /**
     * 表格分格表单
     *
     * @param $title
     * @param Closure $content
     * @param bool $active
     * @return $this
     */
    public function tab($title,Closure $content,$active = false)
    {
        $this->getTab()->append($title,$content,$active);
        return $this;
    }

    /**
     * 获取表
     *
     * @return null|Tab
     */
    public function getTab()
    {
        if(is_null($this->tab)){
            $this->tab = new Tab($this);
        }
        return $this->tab;
    }

    /**
     * 删除
     *
     * @param $id
     * @return bool
     */
    public function destroy($id)
    {
        $ids = explode(',',$id);
        foreach($ids as $id){
            if(empty($id)){
                continue;
            }
            $this->deleteFilesAndImages($id);
            $this->model->find($id)->delete();
        }
        return true;
    }


    /**
     * 删除文件图片
     *
     * @param $id
     */
    protected function deleteFilesAndImages($id)
    {
        $data = $this->model->with($this->getRelations())
            ->findOrFail($id)->toArray();

        $this->builder->fields()->filter(function($field){
            return $field instanceof Field\File;
        })->each(function(File $file) use ($data){
            $file->setOriginal($data);
            $file->destroy();
        });
    }

    /**
     * 保存一个新的记录
     *
     * @return $this
     */
    public function store()
    {
        $data = Input::all();
        //处理验证错误
        if ($validationMessages = $this->validationMessages($data)) {
            return back()->withInput()->withErrors($validationMessages);
        }

        if(($response = $this->prepare($data)) instanceof Response){
            return $response;
        }
        DB::transaction(function () {
            $inserts = $this->prepareInsert($this->updates);

            foreach ($inserts as $column => $value) {
                $this->model->setAttribute($column, $value);
            }

            $this->model->save();

            $this->updateRelation($this->relations);
        });

        if (($response = $this->complete($this->saved)) instanceof Response) {
            return $response;
        }

        if ($response = $this->ajaxResponse(trans('admin.save_succeeded'))) {
            return $response;
        }

        return $this->redirectAfterStore();
    }


    /**
     * 创建新记录后重定向
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    protected function redirectAfterStore()
    {
        admin_toastr(trans('admin.save_succeeded'));

        $url = Input::get(Builder::PREVIOUS_URL_KEY) ?: $this->resource(0);

        return redirect($url);
    }

    /**
     * 获取ajax Response
     *
     * @param $message
     * @return bool
     */
    protected function ajaxResponse($message)
    {
        $request = Request::capture();

        if($request->ajax() && !$request->pjax()){
            return response()->json([
                'status'  => true,
                'message' => $message,
            ]);
        }
        return false;
    }

    /**
     * 处理
     *
     * @param array $data
     * @return mixed
     */
    protected function prepare($data = [])
    {
        if (($response = $this->callSubmitted()) instanceof Response) {
            return $response;
        }
        $this->inputs = $this->removeIgnoredFields($data);

        if (($response = $this->callSaving()) instanceof Response) {
            return $response;
        }

        $this->relations = $this->getRelationInputs($this->inputs);

        $this->updates = array_except($this->inputs, array_keys($this->relations));
    }

    /**
     * 处理更新
     *
     * @param $id
     * @return Response
     */
    public function update($id)
    {
        $data = Input::all();
        $data = $this->handleEditable($data);
        $data = $this->handleFileDelete($data);

        if($this->handleOrderable($id,$data)){
            return response([
                'status' => true,
                'message' => trans('admin.update_successed'),
            ]);
        }

        $this->model = $this->model->with($this->getRelations())->findOrFail($id);
        $this->setFieldOriginalValue();

        //处理验证错误
        if($validationMessages = $this->validationMessages($data)){
            return back()->withInput()->withError($validationMessages);
        }

        if(($response = $this->prepare($data)) instanceof Response){
            return $response;
        }

        DB::transaction(function(){
            $updates = $this->prepareUpdate($this->updates);

            foreach($updates as $column => $value){
                $this->model->setAttribute($column,$value);
            }

            $this->model->save();

            $this->updateRelation($this->relations);
        });

        if(($result = $this->complete($this->saved)) instanceof Response){
            return $result;
        }

        if ($response = $this->ajaxResponse(trans('admin.update_succeeded'))) {
            return $response;
        }

        return $this->redirectAfterUpdate();
    }


    /**
     * 更新后重定向
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    protected function redirectAfterUpdate()
    {
        admin_toastr(trans('admin.update_successed'));
        $url = Input::get(Builder::PREVIOUS_URL_KEY) ?: $this->resource(-1);
        return redirect($url);
    }




    public function render()
    {
        try{
            return $this->builder->render();
        }catch(\Exception $e){
            return Handler::renderException($e);
        }
    }


    /**
     * Get or Set data
     *
     * @param $key
     * @param null $value
     * @return array|mixed
     */
    public function input($key,$value=null)
    {
        if(is_null($value)){
            return array_get($this->inputs,$key);
        }
        return array_set($this->inputs, $key, $value);
    }


    /**
     * 注册内置字段
     */
   public static function registerBuiltinFields()
   {
       $map = [
           'button'            => \Sinta\Laravel\Admin\Form\Field\Button::class,
           'checkbox'          => \Sinta\Laravel\Admin\Form\Field\Checkbox::class,
           'color'             => \Sinta\Laravel\Admin\Form\Field\Color::class,
           'currency'          => \Sinta\Laravel\Admin\Form\Field\Currency::class,
           'date'              => \Sinta\Laravel\Admin\Form\Field\Date::class,
           'dateRange'         => \Sinta\Laravel\Admin\Form\Field\DateRange::class,
           'datetime'          => \Sinta\Laravel\Admin\Form\Field\Datetime::class,
           'dateTimeRange'     => \Sinta\Laravel\Admin\Form\Field\DatetimeRange::class,
           'datetimeRange'     => \Sinta\Laravel\Admin\Form\Field\DatetimeRange::class,
           'decimal'           => \Sinta\Laravel\Admin\Form\Field\Decimal::class,
           'display'           => \Sinta\Laravel\Admin\Form\Field\Display::class,
           'divider'           => \Sinta\Laravel\Admin\Form\Field\Divide::class,
           'divide'            => \Sinta\Laravel\Admin\Form\Field\Divide::class,
           'embeds'            => \Sinta\Laravel\Admin\Form\Field\Embeds::class,
           'editor'            => \Sinta\Laravel\Admin\Form\Field\Editor::class,
           'email'             => \Sinta\Laravel\Admin\Form\Field\Email::class,
           'file'              => \Sinta\Laravel\Admin\Form\Field\File::class,
           'hasMany'           => \Sinta\Laravel\Admin\Form\Field\HasMany::class,
           'hidden'            => \Sinta\Laravel\Admin\Form\Field\Hidden::class,
           'id'                => \Sinta\Laravel\Admin\Form\Field\Id::class,
           'image'             => \Sinta\Laravel\Admin\Form\Field\Image::class,
           'ip'                => \Sinta\Laravel\Admin\Form\Field\Ip::class,
           'map'               => \Sinta\Laravel\Admin\Form\Field\Map::class,
           'mobile'            => \Sinta\Laravel\Admin\Form\Field\Mobile::class,
           'month'             => \Sinta\Laravel\Admin\Form\Field\Month::class,
           'multipleSelect'    => \Sinta\Laravel\Admin\Form\Field\MultipleSelect::class,
           'number'            => \Sinta\Laravel\Admin\Form\Field\Number::class,
           'password'          => \Sinta\Laravel\Admin\Form\Field\Password::class,
           'radio'             => \Sinta\Laravel\Admin\Form\Field\Radio::class,
           'rate'              => \Sinta\Laravel\Admin\Form\Field\Rate::class,
           'select'            => \Sinta\Laravel\Admin\Form\Field\Select::class,
           'slider'            => \Sinta\Laravel\Admin\Form\Field\Slider::class,
           'switch'            => \Sinta\Laravel\Admin\Form\Field\SwitchField::class,
           'text'              => \Sinta\Laravel\Admin\Form\Field\Text::class,
           'textarea'          => \Sinta\Laravel\Admin\Form\Field\Textarea::class,
           'time'              => \Sinta\Laravel\Admin\Form\Field\Time::class,
           'timeRange'         => \Sinta\Laravel\Admin\Form\Field\TimeRange::class,
           'url'               => \Sinta\Laravel\Admin\Form\Field\Url::class,
           'year'              => \Sinta\Laravel\Admin\Form\Field\Year::class,
           'html'              => \Sinta\Laravel\Admin\Form\Field\Html::class,
           'tags'              => \Sinta\Laravel\Admin\Form\Field\Tags::class,
           'icon'              => \Sinta\Laravel\Admin\Form\Field\Icon::class,
           'multipleFile'      => \Sinta\Laravel\Admin\Form\Field\MultipleFile::class,
           'multipleImage'     => \Sinta\Laravel\Admin\Form\Field\MultipleImage::class,
           'captcha'           => \Sinta\Laravel\Admin\Form\Field\Captcha::class,
           'listbox'           => \Sinta\Laravel\Admin\Form\Field\Listbox::class,
       ];

       foreach ($map as $abstract => $class){
           static::extend($abstract,$class);
       }
   }


    /**
     * 扩展字段
     *
     * @param $abstract
     * @param $class
     */
    public static function extend($abstract,$class)
    {
        static::$availableFields[$abstract] = $class;
    }


    /**
     * 去掉
     *
     * @param $abstract
     */
    public static function forget($abstract)
    {
        array_forget(static::$availableFields,$abstract);
    }


    /**
     * 获取字段类
     *
     * @param $method
     * @return bool|mixed
     */
    public static function findFieldClass($method)
    {
        $class = array_get(static::$availableFields,$method);

        if(class_exists($class)){
            return $class;
        }

        return false;
    }


    /**
     * 获取字段值
     *
     * @param $name
     * @return mixed
     */
    public function __get($name)
    {
        return $this->input($name);
    }

    /**
     * 设置字段值
     *
     * @param $name
     * @param $value
     */
    public function __set($name,$value)
    {
        $this->input($name,$value);
    }


    /**
     * 创建一个字段对象
     *
     * @param $method
     * @param $arguments
     * @return mixed
     */
    public function __call($method,$arguments)
    {
        if($className = static::findFieldClass($method)){
            $column = array_get($arguments,0,'');
            $element = new $className($column,array_slice($arguments,1));
            $this->pushField($element);
            return $element;
        }
    }


    /**
     * 渲染表单内容成字符串
     *
     * @return string
     */
    public function __toString()
    {
        return $this->render();
    }
}