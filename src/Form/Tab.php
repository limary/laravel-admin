<?php
namespace Sinta\Hippo\Form;

use Closure;
use Illuminate\Database\Eloquent\Collection;

/**
 * 表格
 *
 * Class Tab
 * @package Sinta\Hippo\From
 */
class Tab
{

    protected $form;


    protected $tabs;


    protected $offset = 0;


    public function __construct(Form $form)
    {
        $this->form = $form;
        $this->tabs = new Collection();
    }


    public function append($title, Closure $content, $active = false)
    {
        $fields = $this->collectFields($content);

        $id = 'form-'.($this->tabs->count() + 1);
        $this->tabs->push(compact('id','title','fields','active'));

        return $this;
    }


    /**
     * 字段收集
     *
     * @param Closure $content
     * @return mixed
     */
    public function collectFields(Closure $content)
    {
        call_user_func($content, $this->form);

        $all = $this->form->getBuilder()->fields();

        $fields = $all->slice($this->offset);

        $this->offset = $all->count();

        return $fields;
    }


    public function getTabs()
    {
        if ($this->tabs->filter(function ($tab) {
            return $tab['active'];
        })->isEmpty()) {
            $first = $this->tabs->first();
            $first['active'] = true;

            $this->tabs->offsetSet(0, $first);
        }

        return $this->tabs;
    }


    public function isEmpty()
    {
        return $this->tabs->isEmpty();
    }
}