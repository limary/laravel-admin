<?php
namespace Sinta\Hippo\Form;

use Closure;

use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\MessageBag;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Validation\Validator;
use Illuminate\Database\Eloquent\Relations\Relation;
use Sinta\Hippo\Contracts\Manager\ManagerInterface;
use Sinta\Hippo\Support\ExceptionHandler;
use Sinta\Sms\Exceptions\Exception;
use Symfony\Component\HttpFoundation\Response;


/**
 * 表单
 *
 * Class Form
 * @package Sinta\Hippo\From
 */
class Form
{
    protected $model;

    protected $validator;

    protected $form_builder;

    protected $submitted;

    protected $saving;

    protected $saved;

    protected $updates = [];


    protected $relations = [];

    protected $inputs = [];


    public static $availableFields = [];

    protected $ignored = [];


    protected static $collectedAssets = [];

    protected $tab = null;

    protected $rows = [];

    protected $specRelations = [];

    protected $updateRelating;


    const REMOVE_FLAG_NAME = '_remove_';

    /**
     * @var ManagerInterface
     */
    protected $manager;

    /**
     * 创建一个表实例
     *
     * Form constructor.
     * @param $model
     * @param Closure $closure
     */
    public function __construct($model, Closure $callback, ManagerInterface $manager)
    {
        $this->model = $model;
        $this->manager = $manager;

        $this->form_builder = new FormBuilder($this);
        $callback($this);
    }

    /**
     * 添加一个字段
     *
     * @param Field $field
     * @return $this
     */
    public function pushField(Field $field)
    {
        $field->setForm($this);
        $this->form_builder->fields()->push($field);
        return $this;
    }


    public function addSpecRelations(array $relations)
    {
        $this->specRelations = array_merge($this->specRelations,$relations);
    }

    /**
     * 获取一个Model
     *
     * @return mixed
     */
    public function model()
    {
        return $this->model;
    }


    /**
     * 获取表单创建器
     *
     * @return FormBuilder
     */
    public function getBuilder()
    {
        return $this->form_builder;
    }


    /**
     * 修改一个资源
     *
     * @param $id
     * @return $this
     */
    public function edit($id)
    {
        $this->form_builder->setMode(FormBuilder::MODE_EDIT);
        //设置源Id
        $this->form_builder->setResourceId($id);

        //设置字段的值
        $this->setFieldValue($id);
        return $this;
    }


    public function view($id)
    {
        $this->form_builder->setModel(FormBuilder::MODE_VIEW);
        $this->form_builder->setResourceId($id);

        $this->setFieldValue($id);
        return $this;
    }


    public function tab($title, Closure $content, $active = false)
    {
        $this->getTab()->append($title, $content, $active);

        return $this;
    }


    public function getTab()
    {
        if (is_null($this->tab)) {
            $this->tab = new Tab($this);
        }

        return $this->tab;
    }


    public function getManager()
    {
        return $this->manager;
    }

    /**
     * 删除资源
     *
     * @param $id
     * @return bool
     */
    public function destroy($id)
    {
        $ids = explode(',',$id);

        foreach ($ids as $id) {
            if (empty($id)) {
                continue;
            }
            $this->deleteFilesAndImages($id);
            $this->model->find($id)->delete();
        }

        return true;
    }


    /**
     * 删除图片文件
     *
     * @param $id
     */
    protected function deleteFilesAndImages($id)
    {
        $data = $this->model->with($this->getRelations())
            ->findOrFail($id)->toArray();

        $this->form_builder->fields()->filter(function ($field) {
            return $field instanceof Field\File;
        })->each(function (File $file) use ($data) {
            $file->setOriginal($data);

            $file->destroy();
        });
    }


    public function store()
    {
        $data = Input::all();

        if($validationMessages = $this->validationMessages($data)){
            return back()->withInput()->withErrors($validationMessages);
        }

        if (($response = $this->prepare($data)) instanceof Response) {
            return $response;
        }

        $inserts = $this->prepareInsert($this->updates);


        foreach ($inserts as $column => $value) {
            $this->model->setAttribute($column, $value);
        }



        DB::transaction(function () {
            $inserts = $this->prepareInsert($this->updates);


            foreach ($inserts as $column => $value) {
                $this->model->setAttribute($column, $value);
            }

            $this->model->save();

            $this->updateRelation($this->relations);
        });

        if (($response = $this->complete($this->saved)) instanceof Response) {
            return $response;
        }

        if ($response = $this->ajaxResponse("保存成功!")) {
            return $response;
        }

        return $this->redirectAfterStore();
    }


    protected function redirectAfterStore()
    {
        admin_toastr("保存成功");
        $url = Input::get(FormBuilder::PREVIOUS_URL_KEY) ?: $this->resource(0);
        return redirect($url);
    }


    protected function ajaxResponse($message)
    {
        $request = Request::capture();

        // ajax but not pjax
        if ($request->ajax() && !$request->pjax()) {
            return response()->json([
                'status'  => true,
                'message' => $message,
            ]);
        }

        return false;
    }

    protected function prepare($data = [])
    {
        if (($response = $this->callSubmitted()) instanceof Response) {
            return $response;
        }

        $this->inputs = $this->removeIgnoredFields($data);

        if (($response = $this->callSaving()) instanceof Response) {
            return $response;
        }

        $this->relations = $this->getRelationInputs($this->inputs);

        $this->updates = array_except($this->inputs, array_keys($this->relations));
    }

    /**
     * 移除Ignored的字段
     *
     * @param $input
     * @return array
     */
    protected function removeIgnoredFields($input)
    {
        array_forget($input, $this->ignored);

        return $input;
    }


    protected function getRelationInputs($inputs = [])
    {
        $relations = [];

        foreach ($inputs as $column => $value) {
            if (method_exists($this->model, $column)) {
                $relation = call_user_func([$this->model, $column]);

                if ($relation instanceof Relation) {
                    $relations[$column] = $value;
                }
            }
        }

        return $relations;
    }


    protected function callUpdateRelating()
    {
        if($this->updateRelating instanceof Closure){
            return call_user_func($this->updateRelating, $this,$this->relations);
        }
    }

    protected function callSubmitted()
    {
        if ($this->submitted instanceof Closure) {
            return call_user_func($this->submitted, $this);
        }
    }

    protected function callSaving()
    {
        if ($this->saving instanceof Closure) {
            return call_user_func($this->saving, $this);
        }
    }


    protected function complete(Closure $callback = null)
    {
        if ($callback instanceof Closure) {
            return $callback($this);
        }
    }


    public function update($id)
    {
        $data = Input::all();
        $data = $this->handleEditable($data);

        $data = $this->handleFileDelete($data);

        if ($this->handleOrderable($id, $data)) {
            return response([
                'status'  => true,
                'message' => "更新成功!",
            ]);
        }

        $this->model = $this->model->with($this->getRelations())->findOrFail($id);

        //设置原始值
        $this->setFieldOriginalValue();

        // Handle validation errors.
        if ($validationMessages = $this->validationMessages($data)) {
            return back()->withInput()->withErrors($validationMessages);
        }

        if (($response = $this->prepare($data)) instanceof Response) {
            return $response;
        }



       DB::transaction(function () {
            $updates = $this->prepareUpdate($this->updates);

            foreach ($updates as $column => $value) {
                $this->model->setAttribute($column, $value);
            }

            $this->model->save();

            $this->updateRelation($this->relations);
        });

        if (($result = $this->complete($this->saved)) instanceof Response) {
            return $result;
        }

        if ($response = $this->ajaxResponse("更新成功!")) {
            return $response;
        }

        return $this->redirectAfterUpdate();
    }


    protected function redirectAfterUpdate()
    {
        admin_toastr("更新成功!");

        $url = Input::get(FormBuilder::PREVIOUS_URL_KEY) ?: $this->resource(-1);

        return redirect($url);
    }


    protected function handleEditable(array $input = [])
    {
        if (array_key_exists('_editable', $input)) {
            $name = $input['name'];
            $value = $input['value'];

            array_forget($input, ['pk', 'value', 'name']);
            array_set($input, $name, $value);
        }

        return $input;
    }

    protected function handleFileDelete(array $input = [])
    {
        if (array_key_exists(Field::FILE_DELETE_FLAG, $input)) {
            $input[Field::FILE_DELETE_FLAG] = $input['key'];
            unset($input['key']);
        }

        Input::replace($input);

        return $input;
    }


    protected function handleOrderable($id, array $input = [])
    {
        if (array_key_exists('_orderable', $input)) {
            $model = $this->model->find($id);

            if ($model instanceof Sortable) {
                $input['_orderable'] == 1 ? $model->moveOrderUp() : $model->moveOrderDown();

                return true;
            }
        }

        return false;
    }

    /**
     * 更新关系数据
     *
     * $relationsData = [
     *      'relationName' => []
     * ]
     *
     *
     * @param $relationsData
     */
    protected function updateRelation($relationsData)
    {

        foreach ($relationsData as $name => $values) {
            if(in_array($name,$this->specRelations)){
                continue;
            }
            if (!method_exists($this->model, $name)) {
                continue;
            }

            $relation = $this->model->$name();

            $oneToOneRelation = $relation instanceof \Illuminate\Database\Eloquent\Relations\HasOne
                    || $relation instanceof \Illuminate\Database\Eloquent\Relations\MorphOne;

            $prepared = $this->prepareUpdate([$name => $values], $oneToOneRelation);


            if (empty($prepared)) {
                continue;
            }


            switch (get_class($relation)) {
                case \Illuminate\Database\Eloquent\Relations\BelongsToMany::class:
                case \Illuminate\Database\Eloquent\Relations\MorphToMany::class:
                    if (isset($prepared[$name])) {
                        $relation->sync($prepared[$name]);
                    }
                    break;
                case \Illuminate\Database\Eloquent\Relations\HasOne::class:

                    $related = $this->model->$name;

                    // if related is empty
                    if (is_null($related)) {
                        $related = $relation->getRelated();
                        $related->{$relation->getForeignKeyName()} = $this->model->{$this->model->getKeyName()};
                    }

                    foreach ($prepared[$name] as $column => $value) {
                        $related->setAttribute($column, $value);
                    }

                    $related->save();
                    break;
                case \Illuminate\Database\Eloquent\Relations\MorphOne::class:
                    $related = $this->model->$name;
                    if (is_null($related)) {
                        $related = $relation->make();
                    }
                    foreach ($prepared[$name] as $column => $value) {
                        $related->setAttribute($column, $value);
                    }
                    $related->save();
                    break;
                case \Illuminate\Database\Eloquent\Relations\HasMany::class:
                case \Illuminate\Database\Eloquent\Relations\MorphMany::class:


                    foreach ($prepared[$name] as $related) {

                        $relation = $this->model()->$name();


                        $keyName = $relation->getRelated()->getKeyName();

                        $instance = $relation->findOrNew(array_get($related, $keyName));

                        if ($related[static::REMOVE_FLAG_NAME] == 1) {
                            $instance->delete();

                            continue;
                        }

                        array_forget($related, static::REMOVE_FLAG_NAME);

                        $instance->fill($related);
                        $instance->save();
                    }

                    break;
            }

        }
        $this->callUpdateRelating();
    }

    /**
     *
     *
     * @param array $updates
     * @param bool $oneToOneRelation
     * @return array
     */
    protected function prepareUpdate(array $updates, $oneToOneRelation = false)
    {
        $prepared = [];

        foreach ($this->form_builder->fields() as $field) {
            $columns = $field->column();

            // If column not in input array data, then continue.
            if (!array_has($updates, $columns)) {
                continue;
            }

            if ($this->invalidColumn($columns, $oneToOneRelation)) {
                continue;
            }

            $value = $this->getDataByColumn($updates, $columns);

            $value = $field->prepare($value);

            if (is_array($columns)) {
                foreach ($columns as $name => $column) {
                    array_set($prepared, $column, $value[$name]);
                }
            } elseif (is_string($columns)) {
                array_set($prepared, $columns, $value);
            }
        }

        return $prepared;
    }


    protected function invalidColumn($columns, $oneToOneRelation = false)
    {
        foreach ((array) $columns as $column) {
            if ((!$oneToOneRelation && Str::contains($column, '.')) ||
                ($oneToOneRelation && !Str::contains($column, '.'))) {
                return true;
            }
        }

        return false;
    }


    protected function prepareInsert($inserts)
    {
        if ($this->isHasOneRelation($inserts)) {
            $inserts = array_dot($inserts);
        }

        foreach ($inserts as $column => $value) {
            if (is_null($field = $this->getFieldByColumn($column))) {
                unset($inserts[$column]);
                continue;
            }

            $inserts[$column] = $field->prepare($value);
        }

        $prepared = [];

        foreach ($inserts as $key => $value) {
            array_set($prepared, $key, $value);
        }

        return $prepared;
    }

    protected function isHasOneRelation($inserts)
    {
        $first = current($inserts);

        if (!is_array($first)) {
            return false;
        }

        if (is_array(current($first))) {
            return false;
        }

        return Arr::isAssoc($first);
    }


    public function submitted(Closure $callback)
    {
        $this->submitted = $callback;
    }

    public function saving(Closure $callback)
    {
        $this->saving = $callback;
    }

    public function saved(Closure $callback)
    {
        $this->saved = $callback;
    }

    public function updateRelating(Closure $callback)
    {
        $this->updateRelating = $callback;
    }


    public function ignore($fields)
    {
        $this->ignored = array_merge($this->ignored, (array) $fields);

        return $this;
    }

    protected function getDataByColumn($data, $columns)
    {
        if (is_string($columns)) {
            return array_get($data, $columns);
        }

        if (is_array($columns)) {
            $value = [];
            foreach ($columns as $name => $column) {
                if (!array_has($data, $column)) {
                    continue;
                }
                $value[$name] = array_get($data, $column);
            }

            return $value;
        }
    }


    protected function getFieldByColumn($column)
    {
        return $this->form_builder->fields()->first(
            function (Field $field) use ($column) {
                if (is_array($field->column())) {
                    return in_array($column, $field->column());
                }

                return $field->column() == $column;
            }
        );
    }


    protected function setFieldOriginalValue()
    {
        $values = $this->model->toArray();

        $this->form_builder->fields()->each(function (Field $field) use ($values) {
            $field->setOriginal($values);
        });
    }

    protected static function doNotSnakeAttributes(Model $model)
    {
        $class = get_class($model);

        $class::$snakeAttributes = false;
    }


    protected function validationMessages($input)
    {
        $failedValidators = [];

        foreach ($this->form_builder->fields() as $field) {
            if (!$validator = $field->getValidator($input)) {
                continue;
            }

            if (($validator instanceof Validator) && !$validator->passes()) {
                $failedValidators[] = $validator;
            }
        }

        $message = $this->mergeValidationMessages($failedValidators);

        return $message->any() ? $message : false;
    }


    protected function mergeValidationMessages($validators)
    {
        $messageBag = new MessageBag();

        foreach ($validators as $validator) {
            $messageBag = $messageBag->merge($validator->messages());
        }

        return $messageBag;
    }


    public function getRelations()
    {
        $relations = $columns = [];

        foreach ($this->form_builder->fields() as $field) {
            if($field instanceof SpecFieldInterface){
                $rels = $field->getRelations();
                $relations = array_merge($relations,$rels);
                $this->addSpecRelations($rels);
            }else{
                $columns[] = $field->column();
            }

        }

        foreach (array_flatten($columns) as $column) {
            if (str_contains($column, '.')) {
                list($relation) = explode('.', $column);

                if (method_exists($this->model, $relation) &&
                    $this->model->$relation() instanceof Relation
                ) {
                    $relations[] = $relation;
                }
            } elseif (method_exists($this->model, $column) &&
                !method_exists(Model::class, $column)
            ) {
                $relations[] = $column;
            }
        }

        return array_unique($relations);
    }


    public function setAction($action)
    {
        $this->form_builder->setAction($action);

        return $this;
    }


    public function setWidth($fieldWidth = 8, $labelWidth = 2)
    {
        $this->form_builder->fields()->each(function ($field) use ($fieldWidth, $labelWidth) {
            $field->setWidth($fieldWidth, $labelWidth);
        });

        $this->form_builder->setWidth($fieldWidth, $labelWidth);

        return $this;
    }

    /**
     * 设置字段值
     *
     * @param $id
     */
    protected function setFieldValue($id)
    {
        $relations = $this->getRelations();



        $this->model = $this->model->with($relations)->findOrFail($id);
        $data = $this->model->toArray();
        //字段填充
        $this->form_builder->fields()->each(function (Field $field) use ($data) {
            $field->fill($data);
        });
    }


    public function setView($view)
    {
        $this->getBuilder()->setView($view);

        return $this;
    }

    public function row(Closure $callback)
    {
        $this->rows[] = new Row($callback, $this);

        return $this;
    }


    public function tools(Closure $callback)
    {
        $callback->call($this, $this->getBuilder()->getTools());
    }

    public function disableSubmit()
    {
        $this->getBuilder()->options(['enableSubmit' => false]);

        return $this;
    }

    public function disableReset()
    {
        $this->getBuilder()->options(['enableReset' => false]);

        return $this;
    }

    public function resource($slice = -2)
    {
        $segments = explode('/', trim(app('request')->getUri(), '/'));

        if ($slice != 0) {
            $segments = array_slice($segments, 0, $slice);
        }

        return implode('/', $segments);
    }


    public function render()
    {
        try {
            return $this->form_builder->render();
        } catch (\Exception $e) {
            return ExceptionHandler::renderException($e);
        }
    }

    public function input($key, $value = null)
    {
        if (is_null($value)) {
            return array_get($this->inputs, $key);
        }

        return array_set($this->inputs, $key, $value);
    }

    public static function registerBuiltinFields()
    {
        $map = [
            'button'         => \Sinta\Hippo\Form\Field\Button::class,
            'checkbox'       => \Sinta\Hippo\Form\Field\Checkbox::class,
            'color'          => \Sinta\Hippo\Form\Field\Color::class,
            'currency'       => \Sinta\Hippo\Form\Field\Currency::class,
            'date'           => \Sinta\Hippo\Form\Field\Date::class,
            'dateRange'      => \Sinta\Hippo\Form\Field\DateRange::class,
            'datetime'       => \Sinta\Hippo\Form\Field\Datetime::class,
            'dateTimeRange'  => \Sinta\Hippo\Form\Field\DatetimeRange::class,
            'datetimeRange'  => \Sinta\Hippo\Form\Field\DatetimeRange::class,
            'decimal'        => \Sinta\Hippo\Form\Field\Decimal::class,
            'display'        => \Sinta\Hippo\Form\Field\Display::class,
            'divide'         => \Sinta\Hippo\Form\Field\Divide::class,
            'embeds'         => \Sinta\Hippo\Form\Field\Embeds::class,
            'editor'         => \Sinta\Hippo\Form\Field\Editor::class,
            'email'          => \Sinta\Hippo\Form\Field\Email::class,
            'file'           => \Sinta\Hippo\Form\Field\File::class,
            'hasMany'        => \Sinta\Hippo\Form\Field\HasMany::class,
            'hidden'         => \Sinta\Hippo\Form\Field\Hidden::class,
            'id'             => \Sinta\Hippo\Form\Field\Id::class,
            'image'          => \Sinta\Hippo\Form\Field\Image::class,
            'ip'             => \Sinta\Hippo\Form\Field\Ip::class,
            'map'            => \Sinta\Hippo\Form\Field\Map::class,
            'mobile'         => \Sinta\Hippo\Form\Field\Mobile::class,
            'month'          => \Sinta\Hippo\Form\Field\Month::class,
            'multipleSelect' => \Sinta\Hippo\Form\Field\MultipleSelect::class,
            'number'         => \Sinta\Hippo\Form\Field\Number::class,
            'password'       => \Sinta\Hippo\Form\Field\Password::class,
            'radio'          => \Sinta\Hippo\Form\Field\Radio::class,
            'rate'           => \Sinta\Hippo\Form\Field\Rate::class,
            'select'         => \Sinta\Hippo\Form\Field\Select::class,
            'slider'         => \Sinta\Hippo\Form\Field\Slider::class,
            'switch'         => \Sinta\Hippo\Form\Field\SwitchField::class,
            'text'           => \Sinta\Hippo\Form\Field\Text::class,
            'textarea'       => \Sinta\Hippo\Form\Field\Textarea::class,
            'time'           => \Sinta\Hippo\Form\Field\Time::class,
            'timeRange'      => \Sinta\Hippo\Form\Field\TimeRange::class,
            'url'            => \Sinta\Hippo\Form\Field\Url::class,
            'year'           => \Sinta\Hippo\Form\Field\Year::class,
            'html'           => \Sinta\Hippo\Form\Field\Html::class,
            'tags'           => \Sinta\Hippo\Form\Field\Tags::class,
            'icon'           => \Sinta\Hippo\Form\Field\Icon::class,
            'multipleFile'   => \Sinta\Hippo\Form\Field\MultipleFile::class,
            'multipleImage'  => \Sinta\Hippo\Form\Field\MultipleImage::class,
            'captcha'        => \Sinta\Hippo\Form\Field\Captcha::class,
            'listbox'        => \Sinta\Hippo\Form\Field\Listbox::class,
        ];

        foreach ($map as $abstract => $class) {
            static::extend($abstract, $class);
        }
    }

    public static function extend($abstract, $class)
    {
        static::$availableFields[$abstract] = $class;
    }


    public static function forget($abstract)
    {
        array_forget(static::$availableFields, $abstract);
    }

    public static function findFieldClass($method)
    {
        $class = array_get(static::$availableFields, $method);

        if (class_exists($class)) {
            return $class;
        }

        return false;
    }


    /**
     * 收集字段的assets
     *
     * @return array
     */
    public static function collectFieldAssets()
    {
        if (!empty(static::$collectedAssets)) {
            return static::$collectedAssets;
        }

        $css = collect();
        $js = collect();

        foreach (static::$availableFields as $field) {
            if (!method_exists($field, 'getAssets')) {
                continue;
            }

            $assets = call_user_func([$field, 'getAssets']);

            $css->push(array_get($assets, 'css'));
            $js->push(array_get($assets, 'js'));
        }

        return static::$collectedAssets = [
            'css' => $css->flatten()->unique()->filter()->toArray(),
            'js'  => $js->flatten()->unique()->filter()->toArray(),
        ];
    }

    public function __get($name)
    {
        return $this->input($name);
    }


    public function __set($name, $value)
    {
        $this->input($name, $value);
    }


    public function __call($method, $arguments)
    {
        if($className = static::findFieldClass($method)){
            $column = array_get($arguments,0,'');
            $element = new $className($column, array_slice($arguments, 1));
            $this->pushField($element);
            return $element;
        }
    }


    public function __toString()
    {
        return $this->render();
    }
}