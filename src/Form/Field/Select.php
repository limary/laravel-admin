<?php
namespace Sinta\Laravel\Admin\Form\Field;

use Sinta\Laravel\Admin\Form\Field;

class Select extends Field
{

    protected static $css = [
        '/vendor/laravel-admin/AdminLTE/plugins/select2/select2.min.css',
    ];

    protected static $js = [
        '/vendor/laravel-admin/AdminLTE/plugins/select2/select2.full.min.js',
    ];


    public function render()
    {
        if(empty($this->script)){
            $this->script = <<<EOF
$("{$this->getElementClassSelector()}").select2({
    allowClear: true,
    placeholder: "{$this->label}"
});
EOF;
        }

        if ($this->options instanceof \Closure) {
            if ($this->form) {
                $this->options = $this->options->bindTo($this->form->model());
            }

            $this->options(call_user_func($this->options, $this->value));
        }

        $this->options = array_filter($this->options);
        return parent::render()->with(['options' => $this->options]);
    }
}