<?php

namespace Sinta\Laravel\Admin\Form\Field;


use Sinta\Laravel\Admin\Form\Field;

class Html extends Field
{

    protected $html = '';

    protected $label = '';

    protected $plain = false;

    public function __construct($html, $arguments)
    {
        $this->html = $html;

        $this->label = array_get($arguments, 0);
    }

    public function plain()
    {
        $this->plain = true;

        return $this;
    }

    public function render()
    {
        if ($this->html instanceof \Closure) {
            $this->html = $this->html->call($this->form->model(), $this->form);
        }

        if ($this->plain) {
            return $this->html;
        }

        $viewClass = $this->getViewElementClasses();

        return <<<EOT
<div class="form-group">
    <label  class="{$viewClass['label']} control-label">{$this->label}</label>
    <div class="{$viewClass['field']}">
        {$this->html}
    </div>
</div>
EOT;
    }
}