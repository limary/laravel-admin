<?php

namespace Sinta\Laravel\Admin\Form\Field;

class TimeRange extends DateRange
{
    protected $format = 'HH:mm:ss';
}