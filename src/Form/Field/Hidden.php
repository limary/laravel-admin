<?php
namespace Sinta\Laravel\Admin\Form\Field;

use Sinta\Laravel\Admin\Form\Field;

/**
 * 隐藏字段
 *
 * Class Hidden
 * @package Sinta\Laravel\Admin\Form\Field
 */
class Hidden extends Field
{

}