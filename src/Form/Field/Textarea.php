<?php
namespace Sinta\Laravel\Admin\Form\Field;

use Sinta\Laravel\Admin\Form\Field;

/**
 * 文本域
 *
 * Class Textarea
 * @package Sinta\Laravel\Admin\Form\Field
 */
class Textarea extends Field
{
    protected $rows = 5;

    public function rows($rows = 5)
    {
        $this->rows = $rows;

        return $this;
    }

    public function render()
    {
        return parent::render()->with(['rows' => $this->rows]);
    }
}