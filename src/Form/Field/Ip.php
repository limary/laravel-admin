<?php
namespace Sinta\Laravel\Admin\Form\Field;


/**
 * IP字段
 *
 * Class Ip
 * @package Sinta\Laravel\Admin\Form\Field
 */
class Ip extends Text
{
    protected $rules = 'ip';

    protected static $js = [
        '/vendor/laravel-admin/AdminLTE/plugins/input-mask/jquery.inputmask.bundle.min.js',
    ];

    protected $options = [
        'alias' => 'ip',
    ];

    public function render()
    {
        $options = json_encode($this->options);

        $this->script = <<<EOT

$('{$this->getElementClassSelector()}').inputmask($options);
EOT;

        $this->prepend('<i class="fa fa-laptop"></i>')
            ->defaultAttribute('style', 'width: 130px');

        return parent::render();
    }
}