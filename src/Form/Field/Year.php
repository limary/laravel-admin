<?php

namespace Sinta\Laravel\Admin\Form\Field;


class Year extends Date
{
    protected $format = 'YYYY';
}