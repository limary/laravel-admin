<?php

namespace Sinta\Laravel\Admin\Form\Field;

class Month extends Date
{
    protected $format = 'MM';
}
