<?php
namespace Sinta\Laravel\Admin\Form\Field;

/**
 * 手机号字段
 *
 * Class Mobile
 * @package Sinta\Laravel\Admin\Form\Field
 */
class Mobile extends Text
{
    protected static $js = [
        '/vendor/laravel-admin/AdminLTE/plugins/input-mask/jquery.inputmask.bundle.min.js',
    ];

    protected $options = [
        'mask' => '99999999999',
    ];


    public function render()
    {
        $options = json_encode($this->options);

        $this->script = <<<EOT

$('{$this->getElementClassSelector()}').inputmask($options);
EOT;

        $this->prepend('<i class="fa fa-phone"></i>')
            ->defaultAttribute('style', 'width: 150px');

        return parent::render();
    }
}