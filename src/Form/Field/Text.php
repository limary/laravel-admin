<?php
namespace Sinta\Laravel\Admin\Form\Field;

use Sinta\Laravel\Admin\Form\Field;

/**
 * 文本字段
 *
 * Class Text
 * @package Sinta\Laravel\Admin\Form\Field
 */
class Text extends Field
{
    use PlainInput;

    public function render()
    {
        $this->initPlainInput();

        $this->prepend('<i class="fa fa-pencil"></i>')
            ->defaultAttribute('type','text')
            ->defaultAttribute('id', $this->id)
            ->defaultAttribute('name', $this->elementName ?: $this->formatName($this->column))
            ->defaultAttribute('value', old($this->column, $this->value()))
            ->defaultAttribute('class', 'form-control '.$this->getElementClassString())
            ->defaultAttribute('placeholder', $this->getPlaceholder());

        return parent::render()->with([
            'prepend' => $this->prepend,
            'append'  => $this->append,
        ]);
    }
}