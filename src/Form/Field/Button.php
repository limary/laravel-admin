<?php
namespace Sinta\Laravel\Admin\Form\Field;

use Sinta\Laravel\Admin\Form\Field;


/**
 * 按键字段
 *
 * Class Button
 * @package Sinta\Laravel\Admin\Form\Field
 */
class Button extends Field
{
    protected $class = 'btn-primary';

    public function info()
    {
        $this->class = 'btn-info';

        return $this;
    }

    public function on($event,$callback)
    {
        $this->script = <<< EOT
        $('{$this->getElementClassSelector()}').on('$event',function(){
            $callback
        });
EOT;
    }
}