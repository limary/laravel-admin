<?php

namespace Sinta\Laravel\Admin\Form\Field;

use Sinta\Laravel\Admin\Form\Field;

class Divide extends Field
{
    public function render()
    {
        return '<hr>';
    }
}