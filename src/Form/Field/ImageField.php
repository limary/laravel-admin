<?php

namespace Sinta\Laravel\Admin\Form\Field;

use Intervention\Image\ImageManagerStatic;

trait ImageField
{
    protected $interventionCalls = [];

    public function defaultDirectory()
    {
        return config('admin.upload.directory.image');
    }

    public function callInterventionMethods($target)
    {
        if (!empty($this->interventionCalls)) {
            $image = ImageManagerStatic::make($target);

            foreach ($this->interventionCalls as $call) {
                call_user_func_array(
                    [$image, $call['method']],
                    $call['arguments']
                )->save($target);
            }
        }

        return $target;
    }

    public function __call($method, $arguments)
    {
        if (!class_exists(ImageManagerStatic::class)) {
            throw new \Exception('To use image handling and manipulation, please install [intervention/image] first.');
        }

        $this->interventionCalls[] = [
            'method'    => $method,
            'arguments' => $arguments,
        ];

        return $this;
    }

    public function render()
    {
        $this->options(['allowedFileTypes' => ['image']]);

        return parent::render();
    }
}