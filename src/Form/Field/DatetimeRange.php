<?php

namespace Sinta\Laravel\Admin\Form\Field;


class DatetimeRange extends DateRange
{
    protected $format = 'YYYY-MM-DD HH:mm:ss';
}