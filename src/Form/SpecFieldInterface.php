<?php
namespace Sinta\Hippo\Form;


interface SpecFieldInterface
{
    public function getRelations():array;
}