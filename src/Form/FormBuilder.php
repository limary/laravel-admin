<?php

namespace Sinta\Hippo\Form;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Str;

/**
 * 表单创建者
 *
 * Class FormBuilder
 * @package Sinta\Hippo\From
 */
class FormBuilder
{
    const PREVIOUS_URL_KEY = '_previous_';


    protected $id;

    /**
     * @var Form
     */
    protected $form;


    protected $action;


    // 要显示的字段
    protected $fields;


    protected $options = [
        'enableSubmit' => true,
        'enableReset'  => true,
    ];

    /**
     * 模式
     */
    const MODE_VIEW = 'view';
    const MODE_EDIT = 'edit';
    const MODE_CREATE = 'create';


    protected $mode = 'create';

    /**
     * 隐藏字段
     *
     * @var array
     */
    protected $hiddenFields = [];


    //表单工具栏
    protected $tools;


    protected $width = [
        'label' => 2,
        'field' => 8,
    ];


    protected $view = 'widgets::form';

    /**
     * 表单创建器
     *
     * FormBuilder constructor.
     * @param Form $form
     */
    public function __construct(Form $form)
    {
        $this->form = $form;

        $this->fields = new Collection();
        $this->setupTools();
    }

    /**
     * 表单获取
     *
     * @return Form
     */
    public function getForm()
    {
        return $this->form;
    }

    /**
     * 设置工具栏
     */
    public function setupTools()
    {
        $this->tools = new Tools($this);
    }


    public function getTools()
    {
        return $this->tools;
    }

    public function setMode($mode = 'create')
    {
        $this->mode = $mode;
    }

    public function isMode($mode)
    {
        return $this->mode == $mode;
    }

    public function setResourceId($id)
    {
        $this->id = $id;
    }

    public function getResource($slice = null)
    {
        if ($this->mode == self::MODE_CREATE) {
            return $this->form->resource(-1);
        }
        if ($slice !== null) {
            return $this->form->resource($slice);
        }

        return $this->form->resource();
    }


    public function setWidth($field = 8, $label = 2)
    {
        $this->width = [
            'label' => $label,
            'field' => $field,
        ];

        return $this;
    }


    public function setAction($action)
    {
        $this->action = $action;
    }


    public function getAction()
    {
        if ($this->action) {
            return $this->action;
        }

        if ($this->isMode(static::MODE_EDIT)) {
            return $this->form->resource().'/'.$this->id;
        }

        if ($this->isMode(static::MODE_CREATE)) {
            return $this->form->resource(-1);
        }

        return '';
    }

    public function setView($view)
    {
        $this->view = $view;

        return $this;
    }


    public function fields()
    {
        return $this->fields;
    }

    public function field($name)
    {
        return $this->fields()->first(function (Field $field) use ($name) {
            return $field->column() == $name;
        });
    }

    public function hasRows()
    {
        return !empty($this->form->rows);
    }

    public function getRows()
    {
        return $this->form->rows;
    }

    public function getHiddenFields()
    {
        return $this->hiddenFields;
    }


    public function addHiddenField(Field $field)
    {
        $this->hiddenFields[] = $field;
    }


    public function options($options = [])
    {
        if (empty($options)) {
            return $this->options;
        }

        $this->options = array_merge($this->options, $options);
    }


    public function option($option, $value = null)
    {
        if (func_num_args() == 1) {
            return array_get($this->options, $option);
        }

        $this->options[$option] = $value;

        return $this;
    }


    public function title()
    {
        if ($this->mode == static::MODE_CREATE) {
            return "创建";
        }

        if ($this->mode == static::MODE_EDIT) {
            return "修改";
        }

        if ($this->mode == static::MODE_VIEW) {
            return "查看";
        }

        return '';
    }


    public function hasFile()
    {
        foreach ($this->fields() as $field) {
            if ($field instanceof Field\File) {
                return true;
            }
        }

        return false;
    }


    protected function addRedirectUrlField()
    {
        $previous = URL::previous();

        if (!$previous || $previous == URL::current()) {
            return;
        }

        if (Str::contains($previous, url($this->getResource()))) {
            $this->addHiddenField((new Field\Hidden(static::PREVIOUS_URL_KEY))->value($previous));
        }
    }


    public function open($options = [])
    {
        $attributes = [];

        if ($this->mode == self::MODE_EDIT) {
            $this->addHiddenField((new Field\Hidden('_method'))->value('PUT'));
        }

        $this->addRedirectUrlField();

        $attributes['action'] = $this->getAction();
        $attributes['method'] = array_get($options, 'method', 'post');
        $attributes['accept-charset'] = 'UTF-8';

        $attributes['class'] = array_get($options, 'class');

        if ($this->hasFile()) {
            $attributes['enctype'] = 'multipart/form-data';
        }

        $html = [];
        foreach ($attributes as $name => $value) {
            $html[] = "$name=\"$value\"";
        }

        return '<form '.implode(' ', $html).' pjax-container>';
    }

    public function close()
    {
        $this->form = null;
        $this->fields = null;

        return '</form>';
    }

    public function submitButton()
    {
        if ($this->mode == self::MODE_VIEW) {
            return '';
        }

        if (!$this->options['enableSubmit']) {
            return '';
        }


        return <<<EOT
<div class="btn-group pull-right">
    <button type="submit" class="btn btn-info pull-right" data-loading-text="<i class='fa fa-spinner fa-spin '></i> 提交">提交</button>
</div>
EOT;
    }


    public function resetButton()
    {
        if (!$this->options['enableReset']) {
            return '';
        }

        return <<<EOT
<div class="btn-group pull-left">
    <button type="reset" class="btn btn-warning">重置</button>
</div>
EOT;
    }




    protected function removeReservedFields()
    {
        if (!$this->isMode(static::MODE_CREATE)) {
            return;
        }

        $reservedColumns = [
            $this->form->model()->getKeyName(),
            $this->form->model()->getCreatedAtColumn(),
            $this->form->model()->getUpdatedAtColumn(),
        ];

        $this->fields = $this->fields()->reject(function (Field $field) use ($reservedColumns) {
            return in_array($field->column(), $reservedColumns);
        });
    }


    public function render()
    {
        $this->removeReservedFields();

        $tabObj = $this->getForm()->getTab();

        if(!$tabObj->isEmpty()){
            $script = <<<'SCRIPT'

var hash = document.location.hash;
if (hash) {
    $('.nav-tabs a[href="' + hash + '"]').tab('show');
}

// Change hash for page-reload
$('.nav-tabs a').on('shown.bs.tab', function (e) {
    history.pushState(null,null, e.target.hash);
});

if ($('.has-error').length) {
    $('.has-error').each(function () {
        var tabId = '#'+$(this).closest('.tab-pane').attr('id');
        $('li a[href="'+tabId+'"] i').removeClass('hide');
    });

    var first = $('.has-error:first').closest('.tab-pane').attr('id');
    $('li a[href="#'+first+'"]').tab('show');
}

SCRIPT;
            $this->getForm()->getManager()->script($script);
        }

        $data = [
            'form'   => $this,
            'tabObj' => $tabObj,
            'width'  => $this->width,
        ];
        return view($this->view, $data)->render();
    }


    public function renderHeaderTools()
    {
        return $this->tools->render();
    }


    public function __toString()
    {
        return $this->render();
    }
}