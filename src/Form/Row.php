<?php
namespace Sinta\Hippo\Form;

use Closure;
use Illuminate\Contracts\Support\Renderable;

class Row implements Renderable
{
    protected $callback;

    protected $form;

    protected $fields = [];

    protected $defaultFieldWidth = 12;


    public function __construct(Closure $callback, Form $form)
    {
        $this->callback = $callback;

        $this->form = $form;

        call_user_func($this->callback, $this);
    }


    public function width($width = 12)
    {
        $this->defaultFieldWidth = $width;

        return $this;
    }


    public function render()
    {
        return view('widgets::form.row', ['fields' => $this->fields]);
    }

    public function __call($method, $arguments)
    {
        $field = $this->form->__call($method, $arguments);

        $field->disableHorizontal();

        $this->fields[] = [
            'width'   => $this->defaultFieldWidth,
            'element' => $field,
        ];

        return $field;
    }

}