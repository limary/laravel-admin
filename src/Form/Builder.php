<?php
namespace Sinta\Laravel\Admin\Form;

use Illuminate\Support\Collection;
use Sinta\Laravel\Admin\Form;

/**
 * 表单创建器
 *
 * Class Builder
 * @package Sinta\Laravel\Admin\Form
 */
class Builder
{

    const PREVIOUS_URL_KEY = '_previous_';

    protected $id;

    protected $form;

    protected $action;

    /**
     * @var Collection
     */
    protected $fields;

    /**
     * 模型常量
     */
    const MODE_VIEW = 'view';
    const MODE_EDIT = 'edit';
    const MODE_CREATE = 'create';

    protected $mode = 'create';

    /**
     * 隐藏字段
     *
     * @var array
     */
    protected $hiddenFields = [];

    /**
     * @var Tools
     */
    protected $tools;

    protected $view = 'admin::form';

    /**
     * 表
     *
     * Builder constructor.
     * @param Form $form
     */
    public function __construct(Form $form)
    {
        $this->form = $form;
        $this->fields = new Collection();
        $this->setupTools();
    }


    public function setupTools()
    {
        $this->tools = new Tools($this);
    }


    public function render()
    {
        $this->removeReservedFields();

        $tabObj = $this->form->getTab();
    }


    public function __toString()
    {
        return $this->render();
    }
}