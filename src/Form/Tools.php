<?php
namespace Sinta\Hippo\Form;


use Illuminate\Contracts\Support\Htmlable;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;


class Tools implements Renderable
{

    protected $form;

    protected $tools;


    protected $options = [
        'enableListButton' => true,
        'enableBackButton' => true,
    ];


    public function __construct(FormBuilder $builder)
    {
        $this->form = $builder;

        $this->tools = new Collection();
    }


    protected function backButton()
    {
        $script = <<<'EOT'
$('.form-history-back').on('click', function (event) {
    event.preventDefault();
    history.back(1);
});
EOT;

        return <<<EOT
<div class="btn-group pull-right" style="margin-right: 10px">
    <a class="btn btn-sm btn-default form-history-back"><i class="fa fa-arrow-left"></i>&nbsp;返回</a>
</div>
EOT;

    }

    protected function listButton()
    {
        $slice = Str::contains($this->form->getResource(0), '/edit') ? null : -1;
        $resource = $this->form->getResource($slice);
        return <<<EOT
<div class="btn-group pull-right" style="margin-right: 10px">
    <a href="$resource" class="btn btn-sm btn-default"><i class="fa fa-list"></i>&nbsp;列表</a>
</div>
EOT;
    }


    public function add($tool)
    {
        $this->tools->push($tool);
        return $this;
    }

    public function disableBackButton()
    {
        $this->options['enableBackButton'] = false;

        return $this;
    }

    public function disableListButton()
    {
        $this->options['enableListButton'] = false;

        return $this;
    }


    public function render()
    {
        if($this->options['enableBackButton']){
            $this->add($this->listButton());
        }

        if ($this->options['enableBackButton']) {
            $this->add($this->backButton());
        }

        return $this->tools->map(function($tool){
            if ($tool instanceof Renderable) {
                return $tool->render();
            }

            if ($tool instanceof Htmlable) {
                return $tool->toHtml();
            }

            return (string) $tool;
        })->implode(' ');
    }
}