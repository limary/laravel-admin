<?php

namespace Sinta\Hippo\From;


use Illuminate\Contracts\Support\Htmlable;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Support\Collection;

/**
 * 表单工具栏
 *
 * Class FormTools
 * @package Sinta\Hippo\From
 */
class FormTools implements Renderable
{
    /**
     * @var FormBuilder
     */
    protected $form_builder;

    /**
     * 工具集合
     *
     * @var Collection
     */
    protected $tools;


    protected $options = [
        'enableListButton' => true,
        'enableBackButton' => true,
    ];

    /**
     * 工具栏实例
     *
     * FormTools constructor.
     * @param FormBuilder $builder
     */
    public function __construct(FormBuilder $builder)
    {
        $this->form_builder = $builder;
        $this->tools = new Collection();
    }


    public function add($tool)
    {
        $this->tools->push($tool);
        return $this;
    }


    public function disableListButton()
    {
        $this->options['enableListButton'] = false;
        return $this;
    }

    public function disableBackButton()
    {
        $this->options['enableBackButton'] = false;
        return $this;
    }


    /**
     * 渲染
     *
     * @return string
     */
    public function render()
    {
        if($this->options['enableListButton']){
            $this->add($this->listButton());
        }

        if ($this->options['enableBackButton']) {
            $this->add($this->backButton());
        }

        return $this->tools->map(function($tool){
            if ($tool instanceof Renderable) {
                return $tool->render();
            }

            if ($tool instanceof Htmlable) {
                return $tool->toHtml();
            }

            return (string) $tool;
        })->implode(' ');
    }
}