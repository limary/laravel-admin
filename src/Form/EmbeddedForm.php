<?php
namespace Sinta\Laravel\Admin\Form;

use Illuminate\Support\Collection;

class EmbeddedForm
{
    protected $parent = null;

    protected $fields;

    protected $original = [];

    protected $column;


    public function __construct($column)
    {
        $this->column = $column;
        $this->fields = new Collection();
    }


    public function fields()
    {
        return $this->fields;
    }

    public function setParent(Form $parent)
    {
        $this->parent = $parent;

        return $this;
    }

    public function setOriginal($data)
    {
        if (empty($data)) {
            return $this;
        }

        if (is_string($data)) {
            $data = json_decode($data, true);
        }

        $this->original = $data;

        return $this;
    }

    public function prepare($input)
    {
        foreach ($input as $key => $record) {
            $this->setFieldOriginalValue($key);
            $input[$key] = $this->prepareValue($key, $record);
        }

        return $input;
    }


    protected function prepareValue($key, $record)
    {
        $field = $this->fields->first(function (Field $field) use ($key) {
            return in_array($key, (array) $field->column());
        });

        if (method_exists($field, 'prepare')) {
            return $field->prepare($record);
        }

        return $record;
    }

    protected function setFieldOriginalValue($key)
    {
        if (array_key_exists($key, $this->original)) {
            $values = $this->original[$key];

            $this->fields->each(function (Field $field) use ($values) {
                $field->setOriginal($values);
            });
        }
    }

    public function fill(array $data)
    {
        $this->fields->each(function (Field $field) use ($data) {
            $field->fill($data);
        });

        return $this;
    }

    protected function formatField(Field $field)
    {
        $jsonKey = $field->column();

        $elementName = $elementClass = $errorKey = '';

        if (is_array($jsonKey)) {
            foreach ($jsonKey as $index => $name) {
                $elementName[$index] = "{$this->column}[$name]";
                $errorKey[$index] = "{$this->column}.$name";
                $elementClass[$index] = "{$this->column}_$name";
            }
        } else {
            $elementName = "{$this->column}[$jsonKey]";
            $errorKey = "{$this->column}.$jsonKey";
            $elementClass = "{$this->column}_$jsonKey";
        }

        $field->setElementName($elementName)
            ->setErrorKey($errorKey)
            ->setElementClass($elementClass);

        return $field;
    }


    public function pushField(Field $field)
    {
        $field = $this->formatField($field);

        $this->fields->push($field);

        return $this;
    }

    public function __call($method, $arguments)
    {
        if ($className = Form::findFieldClass($method)) {
            $column = array_get($arguments, 0, '');

            /** @var Field $field */
            $field = new $className($column, array_slice($arguments, 1));

            $field->setForm($this->parent);

            $this->pushField($field);

            return $field;
        }

        return $this;
    }
}