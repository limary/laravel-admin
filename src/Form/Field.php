<?php
namespace Sinta\Laravel\Admin\Form;


use Sinta\Laravel\Admin\Admin;
use Illuminate\Support\Arr;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Support\Facades\Validator;

/**
 * 表基类
 *
 * Class Field
 * @package Sinta\Laravel\Admin\Form
 */
class Field implements Renderable
{

    /**
     * 文件删除标志
     */
    const FILE_DELETE_FLAG = '_file_del_';

    /**
     * 元素ID
     *
     * @var array| string
     */
    protected $id;



    /**
     * 值
     *
     * @var mixed
     */
    protected $value;


    /**
     * 字段原来值
     *
     * @var mixed
     */
    protected $original;


    /**
     * 默认值
     *
     * @var mixed
     */
    protected $default;

    /**
     * label
     *
     * @var string
     */
    protected $label = '';

    /**
     * 列名
     *
     * @var string
     */
    protected $column = '';

    /**
     * 元素名字
     *
     * @var array
     */
    protected $elementName = [];

    /**
     * 表单元素类
     *
     * @var array
     */
    protected $elementClass = [];

    /**
     * 变量
     *
     * @var array
     */
    protected $variables = [];


    protected $options = [];

    /**
     * 字段验证规则
     *
     * @var string
     */
    protected $rules = '';

    /**
     * 验证消息
     *
     * @var array
     */
    protected $validationMessages = [];

    /**
     * 字段用到css
     *
     * @var array
     */
    protected static $css = [];

    /**
     * 字段用到的js
     *
     * @var array
     */
    protected static $js = [];


    protected $script = '';

    /**
     * 字段属性
     *
     * @var array
     */
    protected $attributes = [];

    /**
     * 父级表
     *
     * @var null
     */
    protected $form = null;

    /**
     * 要渲染的视图
     *
     * @var string
     */
    protected $view = '';

    /**
     * 帮助
     *
     * @var array
     */
    protected $help = [];

    protected $errorKey;

    protected $placeholder;

    protected $width = [
        'label' => 2,
        'field' => 8
    ];


    protected $horizontal = true;

    /**
     * 创建字段实例
     *
     * Field constructor.
     * @param $column
     * @param array $arguments
     */
    public function __construct($column, $arguments = [])
    {
        $this->column = $column;
        $this->label = $this->formatLabel($arguments);
        $this->id = $this->formatId($column);
    }

    /**
     *
     * @return array
     */
    public static function getAssets()
    {
        return [
            'css' => static::$css,
            'js' => static::$js
        ];
    }


    /**
     * 格式ID字段
     *
     * @param $column
     * @return mixed
     */
    protected function formatId($column)
    {
        return str_replace('.','_',$column);
    }

    /**
     * 格式Label字段
     *
     * @param array $arguments
     * @return mixed
     */
    protected function formatLabel($arguments = [])
    {
        $column = is_array($this->column) ? current($this->column) : $this->column;

        $label = isset($arguments[0]) ? $arguments[0] : ucfirst($column);

        return str_replace(['.', '_'], ' ', $label);
    }

    /**
     * 格式名
     *
     * @param $column
     * @return array|mixed|string
     */
    protected function formatName($column)
    {
        if (is_string($column)) {
            $name = explode('.', $column);

            if (count($name) == 1) {
                return $name[0];
            }

            $html = array_shift($name);
            foreach ($name as $piece) {
                $html .= "[$piece]";
            }

            return $html;
        }

        if (is_array($this->column)) {
            $names = [];
            foreach ($this->column as $key => $name) {
                $names[$key] = $this->formatName($name);
            }

            return $names;
        }

        return '';
    }

    /**
     * 设置元素名
     *
     * @param $name
     * @return $this
     */
    public function setElementName($name)
    {
        $this->elementName = $name;
        return $this;
    }


    /**
     * 填充数据到这个字段
     *
     * @param $data
     * @return void
     */
    public function fill($data)
    {
        if(is_array($this->column)){
            foreach($this->column as $key => $column){
                $this->value[$key] = array_get($data,$column);
            }
            return;
        }
        $this->value = array_get($data,$this->column);
    }

    /**
     * 设置原数据到字段中
     *
     * @param $data
     */
    public function setOriginal($data)
    {
        if (is_array($this->column)) {
            foreach ($this->column as $key => $column) {
                $this->original[$key] = array_get($data, $column);
            }

            return;
        }

        $this->original = array_get($data, $this->column);
    }


    /**
     * 设置表单
     *
     * @param Form|null $form
     * @return $this
     */
    public function setForm(Form $form = null)
    {
        $this->form = $form;
        return $this;
    }

    /**
     * 宽设置
     *
     * @param int $field
     * @param int $label
     * @return $this
     */
    public function setWidth($field = 8, $label = 2)
    {
        $this->width = [
            'label' => $label,
            'field' => $field,
        ];

        return $this;
    }

    /**
     * 设置配值
     *
     * @param array $options
     * @return $this
     */
    public function options($options = [])
    {
        if($options instanceof Arrayable){
            $options = $options->toArray();
        }
        $this->options = array_merge($this->options, $options);

        return $this;
    }

    /**
     * 设置验证
     *
     * @param null $rules
     * @param array $messages
     * @return $this
     */
    public function rules($rules = null,$messages =[])
    {
        if ($rules instanceof \Closure) {
            $this->rules = $rules;
        }

        if (is_string($rules)) {
            $rules = array_filter(explode('|', "{$this->rules}|$rules"));

            $this->rules = implode('|', $rules);
        }
        $this->validationMessages = $messages;
        return $this;
    }


    /**
     * 获取验证
     *
     * @return mixed|string
     */
    protected function getRules()
    {
        if($this->rules instanceof \Closure){
            return $this->rules->call($this,$this->form);
        }

        return $this->rules;
    }

    /**
     * 移除
     *
     * @param $rule
     */
    protected function removeRule($rule)
    {
        $this->rules = str_replace($rule, '', $this->rules);
    }

    /**
     * 验证设置
     *
     * @param callable $validator
     * @return $this
     */
    public function validator(callable $validator)
    {
        $this->validator = $validator;

        return $this;
    }


    public function getErrorKey()
    {
        return $this->errorKey ?: $this->column;
    }


    public function setErrorKey($key)
    {
        $this->errorKey = $key;

        return $this;
    }

    /**
     * 字段值
     *
     * @param null $value
     * @return $this|mixed
     */
    public function value($value = null)
    {
        if (is_null($value)) {
            return is_null($this->value) ? $this->getDefault() : $this->value;
        }

        $this->value = $value;

        return $this;
    }


    public function default($default)
    {
        $this->default = $default;

        return $this;
    }

    /**
     * 获取默认值
     *
     * @return mixed
     */
    public function getDefault()
    {
        if ($this->default instanceof \Closure) {
            return call_user_func($this->default, $this->form);
        }

        return $this->default;
    }

    /**
     * 帮助
     *
     * @param string $text
     * @param string $icon
     * @return $this
     */
    public function help($text = '', $icon = 'fa-info-circle')
    {
        $this->help = compact('text', 'icon');

        return $this;
    }



    /**
     * 列
     *
     * @return string
     */
    public function column()
    {
        return $this->column;
    }


    public function label()
    {
        return $this->label;
    }


    public function original()
    {
        return $this->original;
    }


    public function getValidator(array $input)
    {
        if ($this->validator) {
            return $this->validator->call($this, $input);
        }

        $rules = $attributes = [];

        if (!$fieldRules = $this->getRules()) {
            return false;
        }

        if (is_string($this->column)) {
            if (!array_has($input, $this->column)) {
                return false;
            }

            $input = $this->sanitizeInput($input, $this->column);

            $rules[$this->column] = $fieldRules;
            $attributes[$this->column] = $this->label;
        }

        if (is_array($this->column)) {
            foreach ($this->column as $key => $column) {
                if (!array_key_exists($column, $input)) {
                    continue;
                }
                $input[$column.$key] = array_get($input, $column);
                $rules[$column.$key] = $fieldRules;
                $attributes[$column.$key] = $this->label."[$column]";
            }
        }

        return Validator::make($input, $rules, $this->validationMessages, $attributes);
    }


    protected function sanitizeInput($input, $column)
    {
        if ($this instanceof Field\MultipleSelect) {
            $value = array_get($input, $column);
            array_set($input, $column, array_filter($value));
        }

        return $input;
    }


    public function attribute($attribute, $value = null)
    {
        if (is_array($attribute)) {
            $this->attributes = array_merge($this->attributes, $attribute);
        } else {
            $this->attributes[$attribute] = (string) $value;
        }

        return $this;
    }

    public function readOnly()
    {
        return $this->attribute('disabled', true);
    }


    public function placeholder($placeholder = '')
    {
        $this->placeholder = $placeholder;

        return $this;
    }


    public function getPlaceholder()
    {
        return $this->placeholder ?: trans('admin.input').' '.$this->label;
    }

    public function prepare($value)
    {
        return $value;
    }


    protected function formatAttributes()
    {
        $html = [];

        foreach ($this->attributes as $name => $value) {
            $html[] = $name.'="'.e($value).'"';
        }

        return implode(' ', $html);
    }

    /**
     * @return $this
     */
    public function disableHorizontal()
    {
        $this->horizontal = false;

        return $this;
    }


    public function getViewElementClasses()
    {
        if ($this->horizontal) {
            return [
                'label'      => "col-sm-{$this->width['label']}",
                'field'      => "col-sm-{$this->width['field']}",
                'form-group' => 'form-group ',
            ];
        }

        return ['label' => '', 'field' => '', 'form-group' => ''];
    }


    public function setElementClass($class)
    {
        $this->elementClass = (array) $class;

        return $this;
    }

    protected function getElementClass()
    {
        if (!$this->elementClass) {
            $name = $this->elementName ?: $this->formatName($this->column);

            $this->elementClass = (array) str_replace(['[', ']'], '_', $name);
        }

        return $this->elementClass;
    }

    protected function getElementClassString()
    {
        $elementClass = $this->getElementClass();

        if (Arr::isAssoc($elementClass)) {
            $classes = [];

            foreach ($elementClass as $index => $class) {
                $classes[$index] = is_array($class) ? implode(' ', $class) : $class;
            }

            return $classes;
        }

        return implode(' ', $elementClass);
    }


    protected function getElementClassSelector()
    {
        $elementClass = $this->getElementClass();

        if (Arr::isAssoc($elementClass)) {
            $classes = [];

            foreach ($elementClass as $index => $class) {
                $classes[$index] = '.'.(is_array($class) ? implode('.', $class) : $class);
            }

            return $classes;
        }

        return '.'.implode('.', $elementClass);
    }


    public function addElementClass($class)
    {
        if (is_array($class) || is_string($class)) {
            $this->elementClass = array_merge($this->elementClass, (array) $class);

            $this->elementClass = array_unique($this->elementClass);
        }

        return $this;
    }

    /**
     * 移除当前类
     *
     * @param $class
     * @return $this
     */
    public function removeElementClass($class)
    {
        $delClass = [];

        if (is_string($class) || is_array($class)) {
            $delClass = (array) $class;
        }
        foreach ($delClass as $del) {
            if (($key = array_search($del, $this->elementClass))) {
                unset($this->elementClass[$key]);
            }
        }
        return $this;
    }


    /**
     * 字段的变量
     */
    protected function variables()
    {
        return array_merge($this->variables,[
            'id'            => $this->id,
            'name'          => $this->elementName ?: $this->formatName($this->column),
            'help'          => $this->help,
            'class'         => $this->getElementClassString(),
            'value'         => $this->value(),
            'label'         => $this->label,
            'viewClass'     => $this->getViewElementClasses(),
            'column'        => $this->column,
            'errorKey'      => $this->getErrorKey(),
            'attributes'    => $this->formatAttributes(),
            'placeholder'   => $this->getPlaceholder(),
        ]);
    }


    /**
     * 获取视图
     *
     * @return string
     */
    public function getView()
    {
        if(!empty($this->view)){
            return $this->view;
        }
        $class = explode('\\', get_called_class());
        return 'admin::form.'.strtolower(end($class));
    }


    /**
     * 当前Field script
     *
     * @return mixed
     */
    public function getScript()
    {
        return $this->script;
    }


    public function render()
    {
        Admin::script($this->script);
        return view($this->getView(),$this->variables());
    }


    public function __toString()
    {
        return $this->render()->render();
    }

}