<?php
namespace Sinta\Laravel\Admin\Grids;

use Closure;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Support\Collection;
use Sinta\Laravel\Admin\Grids\Tools\AbstractTool;
use Sinta\Laravel\Admin\Grids\Tools\BatchActions;
use Sinta\Laravel\Admin\Grids\Tools\RefreshButton;

/**
 * Grid工具栏
 *
 * Class Tools
 * @package Sinta\Hippo\Grids
 */
class Tools implements Renderable
{

    /**
     * Grid
     *
     * @var Grid
     */
    protected $grid;

    /**
     * 工具集合
     *
     * @var Collection
     */
    protected $tools;


    /**
     *
     *
     * Tools constructor.
     * @param Grid $grid
     */
    public function __construct(Grid $grid)
    {
        $this->grid = $grid;

        $this->tools = new Collection();

        $this->appendDefaultTools();
    }

    /**
     * 添加默认工具
     */
    protected function appendDefaultTools()
    {
        $this->append(new BatchActions($this->grid));

        $this->append(new RefreshButton($this->grid));
    }


    /**
     * 追加工具
     *
     * @param $tool
     * @return $this
     */
    public function append($tool)
    {
        $this->tools->push($tool);

        return $this;
    }


    public function prepend($tool)
    {
        $this->tools->prepend($tool);

        return $this;
    }

    public function disableRefreshButton()
    {
        $this->tools = $this->tools->reject(function ($tool) {
            return $tool instanceof RefreshButton;
        });
    }

    public function disableBatchActions()
    {
        $this->tools = $this->tools->reject(function ($tool) {
            return $tool instanceof BatchActions;
        });
    }

    public function batch(Closure $closure)
    {
        call_user_func($closure, $this->tools->first(function ($tool) {
            return $tool instanceof BatchActions;
        }));
    }

    /**
     * 渲染
     *
     * @return string
     */
    public function render()
    {
        return $this->tools->map(function ($tool) {
            if ($tool instanceof AbstractTool) {
                return $tool->setGrid($this->grid)->render();
            }

            return (string) $tool;
        })->implode(' ');
    }
}