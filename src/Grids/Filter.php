<?php
namespace Sinta\Laravel\Admin\Grids\Filter;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Request;
use Illuminate\Contracts\Support\Renderable;
use Sinta\Hippo\Contracts\Manager\ManagerInterface;
use Sinta\Hippo\Grids\Filter\AbstractFilter;


/**
 * 过滤器
 *
 * Class Filter
 * @package Sinta\Hippo\Grids
 */
class Filter implements Renderable
{
    protected $view = 'widgets::filter.modal';

    /**
     * @var ManagerInterface
     */
    protected $manager;

    protected $action;

    protected $useIdFilter = true;

    protected $filters = [];

    protected $supports = [
        'equal', 'notEqual', 'ilike', 'like', 'gt', 'lt', 'between',
        'where', 'in', 'notIn', 'date', 'day', 'month', 'year', 'hidden',
    ];


    protected $model;

    /**
     * 过滤器构造器
     *
     * Filter constructor.
     * @param Model $model
     */
    public function __construct(Model $model,ManagerInterface $app)
    {
        $this->model = $model;
        $this->manager = $app;

        $pk = $this->model->eloquent()->getKeyName();

        $this->equal($pk, strtoupper($pk));
    }

    /**
     * 设置搜索
     *
     * @param $action
     * @return $this
     */
    public function setAction($action)
    {
        $this->action = $action;

        return $this;
    }


    /**
     * 关闭ID过滤
     */
    public function disableIdFilter()
    {
        $this->useIdFilter = false;
    }


    public function removeIDFilterIfNeeded()
    {
        if (!$this->useIdFilter) {
            array_shift($this->filters);
        }
    }

    /**
     * 过滤条件
     *
     * @return array
     */
    public function conditions()
    {
        $inputs = array_dot(Input::all());

        $inputs = array_filter($inputs, function ($input) {
            return $input !== '' && !is_null($input);
        });
        if (empty($inputs)) {
            return [];
        }

        $params = [];

        foreach ($inputs as $key => $value) {
            array_set($params, $key, $value);
        }

        $conditions = [];
        $this->removeIDFilterIfNeeded();

        foreach ($this->filters() as $filter) {
            $conditions[] = $filter->condition($params);
        }

        return array_filter($conditions);
    }

    public function addFilter(AbstractFilter $filter)
    {
        $filter->setParent($this);

        return $this->filters[] = $filter;
    }

    public function filters()
    {
        return $this->filters;
    }

    public function execute()
    {
        return $this->model->addConditions($this->conditions())->buildData();
    }


    public function render()
    {
        $this->removeIDFilterIfNeeded();

        if (empty($this->filters)) {
            return '';
        }
        $script = <<<'EOT'

$("#filter-modal .submit").click(function () {
    $("#filter-modal").modal('toggle');
    $('body').removeClass('modal-open');
    $('.modal-backdrop').remove();
});

EOT;

        $this->manager->script($script);
        return view($this->view)->with([
            'action'  => $this->action ?: $this->urlWithoutFilters(),
            'filters' => $this->filters,
        ]);
    }


    protected function urlWithoutFilters()
    {
        $columns = [];
        foreach ($this->filters as $filter) {
            $columns[] = $filter->getColumn();
        }

        $request = Request::instance();
        $query = $request->query();
        array_forget($query, $columns);
        $question = $request->getBaseUrl().$request->getPathInfo() == '/' ? '/?' : '?';
        return count($request->query()) > 0
            ? $request->url().$question.http_build_query($query)
            : $request->fullUrl();
    }


    public function __call($method, $arguments)
    {
        if (in_array($method, $this->supports)) {
            $className = '\\Sinta\\Hippo\\Grids\\Filter\\'.ucfirst($method);

            return $this->addFilter(new $className($this->manager,...$arguments));
        }

        return $this;
    }


    public function __toString()
    {
        return $this->render();
    }
}