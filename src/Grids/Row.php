<?php
namespace Sinta\Laravel\Admin\Grids;


class Row
{

    protected $number;

    protected $data;

    protected $attributes = [];


    public function __construct($number, $data)
    {
        $this->number = $number;

        $this->data = $data;
    }


    public function getKey()
    {
        return $this->model->getKey();
    }

    public function getRowAttributes()
    {
        return $this->formatHtmlAttribute($this->attributes);
    }

    public function getColumnAttributes($column)
    {
        if ($attributes = Column::getAttributes($column)) {
            return $this->formatHtmlAttribute($attributes);
        }

        return '';
    }

    private function formatHtmlAttribute($attributes = [])
    {
        $attrArr = [];
        foreach ($attributes as $name => $val) {
            $attrArr[] = "$name=\"$val\"";
        }

        return implode(' ', $attrArr);
    }

    public function setAttributes(array $attributes)
    {
        $this->attributes = $attributes;
    }

    public function style($style)
    {
        if (is_array($style)) {
            $style = implode('', array_map(function ($key, $val) {
                return "$key:$val";
            }, array_keys($style), array_values($style)));
        }

        if (is_string($style)) {
            $this->attributes['style'] = $style;
        }
    }


    public function model()
    {
        return $this->data;
    }

    public function __get($attr)
    {
        return array_get($this->data, $attr);
    }


    public function column($name, $value = null)
    {
        if (is_null($value)) {
            $column = array_get($this->data, $name);

            return $this->dump($column);
        }

        if ($value instanceof \Closure) {
            $value = $value->call($this, $this->column($name));
        }

        array_set($this->data, $name, $value);

        return $this;
    }

    protected function dump($var)
    {
        if (!is_scalar($var)) {
            return '<pre>'.var_export($var, true).'</pre>';
        }

        return $var;
    }

}