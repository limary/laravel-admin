<?php
namespace Sinta\Laravel\Admin\Grids\Exporters;


interface ExporterInterface
{
    public function export();
}