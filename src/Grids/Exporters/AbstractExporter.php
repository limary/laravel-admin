<?php
namespace Sinta\Laravel\Admin\Grids\Exporters;

use Sinta\Laravel\Admin\Grid;
use Sinta\Laravel\Admin\Grids\Exporter;

abstract class AbstractExporter implements ExporterInterface
{
    //var Grid
    protected $grid;


    public function __construct(Grid $grid = null)
    {
        if ($grid) {
            $this->setGrid($grid);
        }
    }

    public function setGrid(Grid $grid)
    {
        $this->grid = $grid;

        return $this;
    }

    public function getTable()
    {
        return $this->grid->model()->eloquent()->getTable();
    }

    public function getData()
    {
        return $this->grid->getFilter()->execute();
    }

    public function chunk(callable $callback, $count = 100)
    {
        return $this->grid->getFilter()->chunk($callback, $count);
    }

    public function withScope($scope)
    {
        if ($scope == Exporter::SCOPE_ALL) {
            return $this;
        }

        list($scope, $args) = explode(':', $scope);

        if ($scope == Exporter::SCOPE_CURRENT_PAGE) {
            $this->grid->model()->usePaginate(true);
        }

        if ($scope == Exporter::SCOPE_SELECTED_ROWS) {
            $selected = explode(',', $args);
            $this->grid->model()->whereIn($this->grid->getKeyName(), $selected);
        }

        return $this;
    }


    abstract public function export();
}