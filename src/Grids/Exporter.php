<?php
namespace Sinta\Laravel\Admin\Grids;


class Exporter
{
    const SCOPE_ALL = 'all';
    const SCOPE_CURRENT_PAGE = 'page';
    const SCOPE_SELECTED_ROWS = 'selected';


    protected $grid;


    protected static $drivers = [];


    public static $queryName = '_export_';


    public function __construct(Grid $grid)
    {
        $this->grid = $grid;

        $this->grid->model()->usePaginate(false);
    }


    public static function setQueryName($name)
    {
        static::$queryName = $name;
    }


    public static function extend($driver, $extend)
    {
        static::$drivers[$driver] = $extend;
    }

    public function resolve($driver)
    {
        if ($driver instanceof Exporters\AbstractExporter) {
            return $driver->setGrid($this->grid);
        }

        return $this->getExporter($driver);
    }

    protected function getExporter($driver)
    {
        if (!array_key_exists($driver, static::$drivers)) {
            return $this->getDefaultExporter();
        }

        return new static::$drivers[$driver]($this->grid);
    }

    public function getDefaultExporter()
    {
        return new Exporters\CsvExporter($this->grid);
    }

    public static function formatExportQuery($scope = '', $args = null)
    {
        $query = '';

        if ($scope == static::SCOPE_ALL) {
            $query = 'all';
        }

        if ($scope == static::SCOPE_CURRENT_PAGE) {
            $query = "page:$args";
        }

        if ($scope == static::SCOPE_SELECTED_ROWS) {
            $query = "selected:$args";
        }

        return [static::$queryName => $query];
    }
}