<?php
namespace Sinta\Laravel\Admin\Grids\Tools;

use Sinta\Laravel\Admin\Grid;

class CreateButton extends AbstractTool
{

    public function __construct(Grid $grid)
    {
        $this->grid = $grid;
    }

    public function render()
    {
        if (!$this->grid->allowCreation()) {
            return '';
        }

        $new = trans('admin.new');
        return <<<EOT
<div class="btn-group pull-right" style="margin-right: 10px">
    <a href="{$this->grid->resource()}/create" class="btn btn-sm btn-success">
        <i class="fa fa-save"></i>&nbsp;&nbsp;{$new}
    </a>
</div>

EOT;
    }
}