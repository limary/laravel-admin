<?php
namespace Sinta\Laravel\Admin\Grids\Tools;

use Sinta\Laravel\Admin\Grid;
use Sinta\Laravel\Admin\Admin;

class ExportButton extends AbstractTool
{

    public function __construct(Grid $grid)
    {
        $this->grid = $grid;
    }


    protected function setUpScripts()
    {
        $script = <<<'SCRIPT'

$('.export-selected').click(function (e) {
    e.preventDefault();
    
    var rows = selectedRows().join(',');
    if (!rows) {
        return false;
    }
    
    var href = $(this).attr('href').replace('__rows__', rows);
    location.href = href;
});

SCRIPT;

        Admin::script($script);
    }


    public function render()
    {
        if (!$this->grid->allowExport()) {
            return '';
        }

        $this->setUpScripts();



        $page = request('page', 1);

        return <<<EOT

<div class="btn-group pull-right" style="margin-right: 10px">
    <a class="btn btn-sm btn-twitter"><i class="fa fa-download"></i> 导出</a>
    <button type="button" class="btn btn-sm btn-twitter dropdown-toggle" data-toggle="dropdown">
        <span class="caret"></span>
        <span class="sr-only">Toggle Dropdown</span>
    </button>
    <ul class="dropdown-menu" role="menu">
        <li><a href="{$this->grid->exportUrl('all')}" target="_blank">所有</a></li>
        <li><a href="{$this->grid->exportUrl('page', $page)}" target="_blank">当前页</a></li>
        <li><a href="{$this->grid->exportUrl('selected', '__rows__')}" target="_blank" class='export-selected'>已选的行</a></li>
    </ul>
</div>
&nbsp;&nbsp;

EOT;
    }

}