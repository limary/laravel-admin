<?php
namespace Sinta\Laravel\Admin\Grids\Tools;


use Sinta\Laravel\Admin\Grid;
use Sinta\Laravel\Admin\Admin;

class PerPageSelector extends AbstractTool
{
    protected $perPage;

    protected $perPageName = '';

    public function __construct(Grid $grid)
    {
        $this->grid = $grid;

        $this->initialize();
    }

    protected function initialize()
    {
        $this->perPageName = $this->grid->model()->getPerPageName();

        $this->perPage = (int) app('request')->input(
            $this->perPageName,
            $this->grid->perPage
        );
    }


    public function getOptions()
    {
        return collect($this->grid->perPages)
            ->push($this->grid->perPage)
            ->push($this->perPage)
            ->unique()
            ->sort();
    }

    public function render()
    {
        Admin::script($this->script());

        $options = $this->getOptions()->map(function ($option) {
            $selected = ($option == $this->perPage) ? 'selected' : '';
            $url = app('request')->fullUrlWithQuery([$this->perPageName => $option]);

            return "<option value=\"$url\" $selected>$option</option>";
        })->implode("\r\n");

        return <<<EOT

<label class="control-label pull-right" style="margin-right: 10px; font-weight: 100;">

        <small>显示</small>&nbsp;
        <select class="input-sm grid-per-pager" name="per-page">
            $options
        </select>
        &nbsp;<small>条目</small>
    </label>

EOT;
    }

    protected function script()
    {
        return <<<EOT
$('.grid-per-pager').on("change", function(e) {
    $.pjax({url: this.value, container: '#pjax-container'});
});
EOT;

    }
}