<?php
namespace Sinta\Laravel\Admin\Grids\Tools;

use Sinta\Laravel\Admin\Grid;
use Illuminate\Contracts\Support\Renderable;

/**
 * 抽像工具类
 *
 * Class AbstractTool
 * @package Sinta\Laravel\Admin\Grids\Tools
 */
abstract class AbstractTool implements Renderable
{

    protected $grid;


    public function setGrid(Grid $grid)
    {
        $this->grid = $grid;
        return $this;
    }


    public function getGrid()
    {
        return $this->grid;
    }

    abstract public function render();


    public function __toString()
    {
        return $this->render();
    }
}