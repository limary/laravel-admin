<?php
namespace Sinta\Laravel\Admin\Grids\Tools;

use Sinta\Laravel\Admin\Admin;


class RefreshButton extends AbstractTool
{

    protected function script()
    {
        return <<<EOT
$('.grid-refresh').on('click', function() {
    $.pjax.reload('#pjax-container');
    toastr.success('刷新成功!');
});
EOT;

    }


    public function render()
    {
        Admin::script($this->script());

        return <<<EOT
<a class="btn btn-sm btn-primary grid-refresh"><i class="fa fa-refresh"></i> </a>
EOT;
    }
}