<?php

namespace Sinta\Laravel\Admin\Grids\Tools;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Input;
use Sinta\Laravel\Admin\Grid;

class Paginator extends AbstractTool
{
    protected $paginator = null;

    public function __construct(Grid $grid)
    {
        parent::__construct($grid);

        $this->initPaginator();
    }

    protected function initPaginator()
    {
        $this->paginator = $this->grid->model()->eloquent();

        if ($this->paginator instanceof LengthAwarePaginator) {
            $this->paginator->appends(Input::all());
        }
    }

    protected function paginationLinks()
    {
        return $this->paginator->render('admin::pagination');
    }

    protected function perPageSelector()
    {
        return new PerPageSelector($this->grid);
    }


    protected function paginationRanger()
    {
        $parameters = [
            'first' => $this->paginator->firstItem(),
            'last'  => $this->paginator->lastItem(),
            'total' => $this->paginator->total(),
        ];

        $parameters = collect($parameters)->flatMap(function ($parameter, $key) {
            return [$key => "<b>$parameter</b>"];
        });

        return trans('admin.pagination.range', $parameters->all());
    }

    public function render()
    {
        if (!$this->grid->usePagination()) {
            return '';
        }

        return $this->paginationRanger().
        $this->paginationLinks().
        $this->perPageSelector();
    }
}