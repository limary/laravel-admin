<?php
namespace Sinta\Laravel\Admin\Grids\Tools;


use Illuminate\Support\Collection;
use Sinta\Laravel\Admin\Admin;


class BatchActions extends AbstractTool
{
    protected $actions;

    protected $enableDelete = true;

    public function __construct()
    {
        $this->actions = new Collection();

        $this->appendDefaultAction();
    }


    protected function appendDefaultAction()
    {
        $this->add("删除", new BatchDelete());
    }

    public function disableDelete()
    {
        $this->enableDelete = false;

        return $this;
    }

    public function add($title, BatchAction $abstract)
    {
        $id = $this->actions->count();

        $abstract->setId($id);

        $this->actions->push(compact('id', 'title', 'abstract'));

        return $this;
    }

    protected function setUpScripts()
    {
        Admin::script($this->script());

        foreach ($this->actions as $action) {
            $abstract = $action['abstract'];
            $abstract->setResource($this->grid->resource());

            Admin::script($abstract->script());
        }
    }

    protected function script()
    {
        return <<<'EOT'

$('.grid-select-all').iCheck({checkboxClass:'icheckbox_minimal-blue'});

$('.grid-select-all').on('ifChanged', function(event) {
    if (this.checked) {
        $('.grid-row-checkbox').iCheck('check');
    } else {
        $('.grid-row-checkbox').iCheck('uncheck');
    }
});

var selectedRows = function () {
    var selected = [];
    $('.grid-row-checkbox:checked').each(function(){
        selected.push($(this).data('id'));
    });

    return selected;
}

EOT;
    }


    public function render()
    {
        if (!$this->enableDelete) {
            $this->actions->shift();
        }

        if ($this->actions->isEmpty()) {
            return '';
        }

        $this->setUpScripts();

        return view('admin::grid.batch-actions', ['actions' => $this->actions])->render();
    }
}