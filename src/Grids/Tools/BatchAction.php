<?php
namespace Sinta\Laravel\Admin\Grids\Tools;

/**
 * 抽象批处理
 *
 * Class BatchAction
 * @package Sinta\Hippo\Grids\Tools
 */
abstract class BatchAction
{

    protected $id;

    protected $resource;


    public function setId($id)
    {
        $this->id = $id;
    }

    public function setResource($resource)
    {
        $this->resource = $resource;
    }

    public function getToken()
    {
        return csrf_token();
    }

    protected function getElementClass()
    {
        return '.grid-batch-'.$this->id;
    }

    public function script()
    {
        return '';
    }
}