<?php
namespace Sinta\Laravel\Admin\Grids;

use Closure;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Str;
use Sinta\Laravel\Admin\Grids\Displayers\AbstractDisplayer;

/**
 * 列
 *
 * Class Column
 * @package Sinta\Hippo\Grids
 */
class Column
{
    protected $grid;

    protected $name;

    protected $label;

    protected $original;

    protected $sortable = false;

    protected $sort;

    protected $attributes = [];

    protected $relation = false;

    protected $relationColumn;

    //Grid原数据
    protected static $originalGridData = [];

    //显示回调
    protected $displayCallbacks = [];


    public static $displayers = [];


    public static $defined = [];


    protected static $htmlAttributes = [];


    protected static $model;

    const SELECT_COLUMN_NAME = '__row_selector__';


    public function __construct($name, $label)
    {
        $this->name = $name;

        $this->label = $this->formatLabel($label);
    }

    public static function extend($name, $displayer)
    {
        static::$displayers[$name] = $displayer;
    }

    public static function define($name, $definition)
    {
        static::$defined[$name] = $definition;
    }

    public function setGrid(Grid $grid)
    {
        $this->grid = $grid;

        $this->setModel($grid->model()->eloquent());
    }


    public function setModel($model)
    {
        if (is_null(static::$model) && ($model instanceof Model)) {
            static::$model = $model->newInstance();
        }
    }

    /**
     * 设置格式原数据
     *
     * @param array $input
     */
    public static function setOriginalGridData(array $input)
    {
        static::$originalGridData = $input;
    }

    /**
     * 设置属性
     *
     * @param array $attributes
     * @return $this
     */
    public function setAttributes($attributes = [])
    {
        static::$htmlAttributes[$this->name] = $attributes;

        return $this;
    }

    public static function getAttributes($name)
    {
        return array_get(static::$htmlAttributes, $name, '');
    }

    public function style($style)
    {
        return $this->setAttributes(compact('style'));
    }

    public function getName()
    {
        return $this->name;
    }

    protected function formatLabel($label)
    {
        $label = $label ?: ucfirst($this->name);

        return str_replace(['.', '_'], ' ', $label);
    }

    public function getLabel()
    {
        return $this->label;
    }

    public function setRelation($relation, $relationColumn = null)
    {
        $this->relation = $relation;
        $this->relationColumn = $relationColumn;

        return $this;
    }

    protected function isRelation()
    {
        return (bool) $this->relation;
    }

    public function sortable()
    {
        $this->sortable = true;

        return $this;
    }

    public function display(Closure $callback)
    {
        $this->displayCallbacks[] = $callback;

        return $this;
    }

    protected function hasDisplayCallbacks()
    {
        return !empty($this->displayCallbacks);
    }

    protected function callDisplayCallbacks($value, $key)
    {
        foreach ($this->displayCallbacks as $callback) {
            $callback = $this->bindOriginalRow($callback, $key);
            $value = call_user_func($callback, $value);
        }

        return $value;
    }

    protected function bindOriginalRow(Closure $callback, $key)
    {
        $originalRow = static::$originalGridData[$key];

        return $callback->bindTo(static::$model->newFromBuilder($originalRow));
    }

    /**
     * 数据填 充
     *
     * @param array $data
     * @return array
     */
    public function fill(array $data)
    {
        foreach ($data as $key => &$row) {
            $this->original = $value = array_get($row, $this->name);

            $value = $this->htmlEntityEncode($value);

            array_set($row, $this->name, $value);

            if ($this->isDefinedColumn()) {
                $this->useDefinedColumn();
            }

            if ($this->hasDisplayCallbacks()) {
                $value = $this->callDisplayCallbacks($this->original, $key);
                array_set($row, $this->name, $value);
            }
        }

        return $data;
    }


    protected function isDefinedColumn()
    {
        return array_key_exists($this->name, static::$defined);
    }

    protected function useDefinedColumn()
    {
        // clear all display callbacks.
        $this->displayCallbacks = [];

        $class = static::$defined[$this->name];

        if ($class instanceof Closure) {
            $this->display($class);

            return;
        }

        if (!class_exists($class) || !is_subclass_of($class, AbstractDisplayer::class)) {
            throw new \Exception("Invalid column definition [$class]");
        }

        $grid = $this->grid;
        $column = $this;

        $this->display(function ($value) use ($grid, $column, $class) {
            $definition = new $class($value, $grid, $column, $this);

            return $definition->display();
        });
    }

    protected function htmlEntityEncode($item)
    {
        if (is_array($item)) {
            array_walk_recursive($item, function (&$value) {
                $value = htmlentities($value);
            });
        } else {
            $item = htmlentities($item);
        }

        return $item;
    }

    public function sorter()
    {
        if (!$this->sortable) {
            return;
        }

        $icon = 'fa-sort';
        $type = 'desc';

        if ($this->isSorted()) {
            $type = $this->sort['type'] == 'desc' ? 'asc' : 'desc';
            $icon .= "-amount-{$this->sort['type']}";
        }

        $query = app('request')->all();
        $query = array_merge($query, [$this->grid->model()->getSortName() => ['column' => $this->name, 'type' => $type]]);

        $url = URL::current().'?'.http_build_query($query);

        return "<a class=\"fa fa-fw $icon\" href=\"$url\"></a>";
    }

    protected function isSorted()
    {
        $this->sort = app('request')->get($this->grid->model()->getSortName());

        if (empty($this->sort)) {
            return false;
        }

        return isset($this->sort['column']) && $this->sort['column'] == $this->name;
    }


    protected function resolveDisplayer($abstract, $arguments)
    {
        if (array_key_exists($abstract, static::$displayers)) {
            return $this->callBuiltinDisplayer(static::$displayers[$abstract], $arguments);
        }

        return $this->callSupportDisplayer($abstract, $arguments);
    }

    protected function callSupportDisplayer($abstract, $arguments)
    {
        return $this->display(function ($value) use ($abstract, $arguments) {
            if (is_array($value) || $value instanceof Arrayable) {
                return call_user_func_array([collect($value), $abstract], $arguments);
            }

            if (is_string($value)) {
                return call_user_func_array([Str::class, $abstract], array_merge([$value], $arguments));
            }

            return $value;
        });
    }


    protected function callBuiltinDisplayer($abstract, $arguments)
    {
        if ($abstract instanceof Closure) {
            return $this->display(function ($value) use ($abstract, $arguments) {
                return $abstract->call($this, ...array_merge([$value], $arguments));
            });
        }

        if (class_exists($abstract) && is_subclass_of($abstract, AbstractDisplayer::class)) {
            $grid = $this->grid;
            $column = $this;

            return $this->display(function ($value) use ($abstract, $grid, $column, $arguments) {
                $displayer = new $abstract($value, $grid, $column, $this);

                return call_user_func_array([$displayer, 'display'], $arguments);
            });
        }

        return $this;
    }


    public function __call($method, $arguments)
    {
        if ($this->isRelation() && !$this->relationColumn) {
            $this->name = "{$this->relation}.$method";
            $this->label = isset($arguments[0]) ? $arguments[0] : ucfirst($method);

            $this->relationColumn = $method;

            return $this;
        }

        return $this->resolveDisplayer($method, $arguments);
    }
}