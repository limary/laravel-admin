<?php
namespace Sinta\Laravel\Admin\Grids\Displayers;

/**
 * 操作列
 *
 * Class Actions
 * @package Sinta\Hippo\Grids\Displayers
 */
class Actions extends AbstractDisplayer
{
    protected $appends = [];

    protected $prepends = [];

    protected $allowEdit = true;

    protected $allowDelete = true;

    protected $resource;

    protected $key;


    public function append($action)
    {
        array_push($this->appends, $action);

        return $this;
    }


    public function prepend($action)
    {
        array_unshift($this->prepends, $action);

        return $this;
    }

    public function disableDelete()
    {
        $this->allowDelete = false;
    }

    public function disableEdit()
    {
        $this->allowEdit = false;
    }

    public function setResource($resource)
    {
        $this->resource = $resource;
    }


    public function getResource()
    {
        return $this->resource ?: parent::getResource();
    }

    /**
     * 显示
     *
     * @param null $callback
     * @return string
     */
    public function display($callback = null)
    {
        if ($callback instanceof \Closure) {
            $callback->call($this, $this);
        }

        $actions = $this->prepends;
        if ($this->allowEdit) {
            array_push($actions, $this->editAction());
        }

        if ($this->allowDelete) {
            array_push($actions, $this->deleteAction());
        }

        $actions = array_merge($actions, $this->appends);

        return implode('', $actions);
    }

    public function setKey($key)
    {
        $this->key = $key;

        return $this;
    }

    public function getKey()
    {
        if ($this->key) {
            return $this->key;
        }

        return parent::getKey();
    }


    protected function editAction()
    {
        return <<<EOT
<a href="{$this->getResource()}/{$this->getKey()}/edit">
    <i class="fa fa-edit"></i>
</a>
EOT;
    }

    protected function deleteAction()
    {
        $script = <<<SCRIPT

$('.grid-row-delete').unbind('click').click(function() {

    var id = $(this).data('id');

    swal({
      title: "确认删除吗?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "确认",
      closeOnConfirm: false,
      cancelButtonText: "取消"
    },
    function(){
        $.ajax({
            method: 'post',
            url: '{$this->getResource()}/' + id,
            data: {
                _method:'delete',
                _token:LA.token,
            },
            success: function (data) {
                $.pjax.reload('#pjax-container');

                if (typeof data === 'object') {
                    if (data.status) {
                        swal(data.message, '', 'success');
                    } else {
                        swal(data.message, '', 'error');
                    }
                }
            }
        });
    });
});

SCRIPT;

        $this->grid->getManager()->script($script);

        return <<<EOT
<a href="javascript:void(0);" data-id="{$this->getKey()}" class="grid-row-delete">
    <i class="fa fa-trash"></i>
</a>
EOT;
    }
}