<?php
namespace Sinta\Laravel\Admin\Grids\Displayers;


use Sinta\Laravel\Admin\Grids\Column;
use Sinta\Laravel\Admin\Grids\Grid;

/**
 * 显示
 *
 * Class AbstractDisplayer
 * @package Sinta\Hippo\Grids\Displayers
 */
abstract class AbstractDisplayer
{
    protected $grid;

    protected $column;

    public $row;

    protected $value;


    public function __construct($value, Grid $grid, Column $column, $row)
    {
        $this->value = $value;
        $this->grid = $grid;
        $this->column = $column;
        $this->row = $row;
    }

    public function getKey()
    {
        return $this->row->{$this->grid->getKeyName()};
    }


    public function getResource()
    {
        return $this->grid->resource();
    }

    abstract public function display();
}