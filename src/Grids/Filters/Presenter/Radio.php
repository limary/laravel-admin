<?php

namespace Sinta\Hippo\Grids\Filter\Presenter;


use Illuminate\Contracts\Support\Arrayable;

class Radio extends Presenter
{

    protected $options = [];

    protected $inline = true;

    public function __construct($options = [],$grid)
    {
        parent::__construct($grid);

        if ($options instanceof Arrayable) {
            $options = $options->toArray();
        }

        $this->options = (array) $options;

        return $this;
    }

    public function stacked() : self
    {
        $this->inline = false;

        return $this;
    }

    protected function prepare()
    {
        $script = "$('.{$this->filter->getId()}').iCheck({radioClass:'iradio_minimal-blue'});";

        $this->getManager()->script($script);
    }

    public function variables() : array
    {
        $this->prepare();

        return [
            'options' => $this->options,
            'inline'  => $this->inline,
        ];
    }
}