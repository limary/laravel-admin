<?php

namespace Sinta\Hippo\Grids\Filter\Presenter;


class DateTime extends Presenter
{

    protected $options = [];

    protected $format = 'YYYY-MM-DD HH:mm:ss';


    public function __construct($options = [],$grid)
    {
        parent::__construct($grid);
        $this->options = $this->getOptions($options);
    }

    protected function getOptions(array  $options) : array
    {
        $options['format'] = array_get($options, 'format', $this->format);
        $options['locale'] = array_get($options, 'locale', config('app.locale'));

        return $options;
    }

    protected function prepare()
    {
        $script = "$('#{$this->filter->getId()}').datetimepicker(".json_encode($this->options).');';

        $this->getManager()->script($script);
    }

    public function variables() : array
    {
        $this->prepare();

        return [];
    }
}