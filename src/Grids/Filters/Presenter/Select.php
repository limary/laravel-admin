<?php

namespace Sinta\Hippo\Grids\Filter\Presenter;


use Illuminate\Contracts\Support\Arrayable;

class Select extends Presenter
{
    protected $options = [];

    public function __construct($options,$grid)
    {
        parent::__construct($grid);
        $this->options = $options;
    }

    protected function buildOptions() : array
    {
        if (is_string($this->options)) {
            $this->loadAjaxOptions($this->options);

            return [];
        }

        if ($this->options instanceof \Closure) {
            $this->options = $this->options->call($this->filter, $this->filter->getValue());
        }

        if ($this->options instanceof Arrayable) {
            $this->options = $this->options->toArray();
        }

        $placeholder = trans('admin.choose');

        $script = <<<SCRIPT
$(".{$this->getElementClass()}").select2({
  placeholder: "$placeholder"
});

SCRIPT;

        $this->getManager()->script($script);

        $options = is_array($this->options) ? $this->options : [];

        return $options;
    }


    protected function loadAjaxOptions($resourceUrl)
    {
        $placeholder = trans('admin.choose');

        $script = <<<EOT

$(".{$this->getElementClass()}").select2({
  placeholder: "$placeholder",
  ajax: {
    url: "$resourceUrl",
    dataType: 'json',
    delay: 250,
    data: function (params) {
      return {
        q: params.term,
        page: params.page
      };
    },
    processResults: function (data, params) {
      params.page = params.page || 1;

      return {
        results: data.data,
        pagination: {
          more: data.next_page_url
        }
      };
    },
    cache: true
  },
  minimumInputLength: 1,
  escapeMarkup: function (markup) {
      return markup;
  }
});

EOT;

        $this->getManager()->script($script);
    }

    public function variables() : array
    {
        return [
            'options' => $this->buildOptions(),
            'class'   => $this->getElementClass(),
        ];
    }


    protected function getElementClass() : string
    {
        return str_replace('.', '_', $this->filter->getColumn());
    }


    public function load($target, $resourceUrl, $idField = 'id', $textField = 'text') : self
    {
        $column = $this->filter->getColumn();

        $script = <<<EOT

$(document).on('change', ".{$this->getClass($column)}", function () {
    var target = $(this).closest('form').find(".{$this->getClass($target)}");
    $.get("$resourceUrl?q="+this.value, function (data) {
        target.find("option").remove();
        $.each(data, function (i, item) {
            $(target).append($('<option>', {
                value: item.$idField,
                text : item.$textField
            }));
        });
        
        $(target).trigger('change');
    });
});
EOT;

        $this->getManager()->script($script);

        return $this;
    }

    protected function getClass($target) : string
    {
        return str_replace('.', '_', $target);
    }
}