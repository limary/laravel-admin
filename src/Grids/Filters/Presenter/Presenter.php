<?php
namespace Sinta\Hippo\Grids\Filter\Presenter;


use Sinta\Hippo\Contracts\Manager\ManagerInterface;
use Sinta\Hippo\Grids\Filter\AbstractFilter;

abstract class Presenter
{
    protected $filter;

    protected $manager;

    public function __construct(ManagerInterface $manager)
    {
        $this->manager = $manager;
    }


    public function setParent(AbstractFilter $filter)
    {
        $this->filter = $filter;
    }


    public function view() : string
    {
        $reflect = new \ReflectionClass(get_called_class());

        return 'widgets::filter.'.strtolower($reflect->getShortName());
    }


    public function getManager()
    {
        return $this->manager;
    }


    public function default($default)
    {
        $this->filter->default($default);

        return $this;
    }

    public function variables() : array
    {
        return [];
    }
}