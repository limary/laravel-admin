<?php

namespace Sinta\Hippo\Grids\Filter\Presenter;


class Text extends Presenter
{

    protected $placeholder = '';


    protected $icon = 'pencil';


    protected $type = 'text';

    public function __construct($placeholder = '',$grid)
    {
        parent::__construct($grid);
        $this->placeholder($placeholder);
    }

    public function variables() : array
    {
        return [
            'placeholder' => $this->placeholder,
            'icon'        => $this->icon,
            'type'        => $this->type,
        ];
    }

    public function placeholder($placeholder = '') : self
    {
        $this->placeholder = $placeholder;

        return $this;
    }

    public function url() : self
    {
        return $this->inputmask(['alias' => 'url'], 'internet-explorer');
    }

    public function email() : self
    {
        return $this->inputmask(['alias' => 'email'], 'envelope');
    }

    public function integer() : self
    {
        return $this->inputmask(['alias' => 'integer']);
    }

    public function decimal($options = []) : self
    {
        return $this->inputmask(array_merge($options, ['alias' => 'decimal']));
    }

    public function currency($options = []) : self
    {
        return $this->inputmask(array_merge($options, [
            'alias'              => 'currency',
            'prefix'             => '',
            'removeMaskOnSubmit' => true,
        ]));
    }

    public function percentage($options = [])
    {
        $options = array_merge(['alias' => 'percentage'], $options);

        return $this->inputmask($options);
    }

    public function ip() : self
    {
        return $this->inputmask(['alias' => 'ip'], 'laptop');
    }

    public function mac() : self
    {
        return $this->inputmask(['alias' => 'mac'], 'laptop');
    }

    public function mobile($mask = '19999999999') : self
    {
        return $this->inputmask(compact('mask'), 'phone');
    }

    public function inputmask($options = [], $icon = 'pencil') : self
    {
        $options = json_encode($options);

        $this->getManager()->script("$('#filter-modal input.{$this->filter->getId()}').inputmask($options);");

        $this->icon = $icon;
        return $this;
    }

}