<?php

namespace Sinta\Hippo\Grids\Filter\Presenter;


class Checkbox extends Radio
{
    protected function prepare()
    {
        $script = "$('.{$this->filter->getId()}').iCheck({checkboxClass:'icheckbox_minimal-blue'});";

        $this->getManager()->script($script);
    }
}