<?php
namespace Sinta\Hippo\Grids\Filter;


class Month extends Date
{
    protected $query = 'whereMonth';

    protected $fieldName = 'month';
}