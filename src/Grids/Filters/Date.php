<?php
namespace Sinta\Hippo\Grids\Filter;


class Date extends AbstractFilter
{
    protected $query = 'whereDate';

    protected $fieldName = 'date';

    public function __construct($column, $label = '')
    {
        parent::__construct($column, $label);

        $this->{$this->fieldName}();
    }
}