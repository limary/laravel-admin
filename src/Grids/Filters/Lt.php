<?php

namespace Sinta\Hippo\Grids\Filter;


class Lt extends AbstractFilter
{
    protected $view = 'widgets::filter.lt';


    public function condition($inputs)
    {
        $value = array_get($inputs, $this->column);

        if (is_null($value)) {
            return;
        }

        $this->value = $value;

        return $this->buildCondition($this->column, '<=', $this->value);
    }
}