<?php

namespace Sinta\Hippo\Grids\Filter;


class Gt extends AbstractFilter
{
    protected $view = 'widgets::filter.gt';


    public function condition($inputs)
    {
        $value = array_get($inputs, $this->column);

        if (is_null($value)) {
            return;
        }

        $this->value = $value;

        return $this->buildCondition($this->column, '>=', $this->value);
    }
}