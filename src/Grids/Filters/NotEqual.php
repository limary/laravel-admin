<?php
namespace Sinta\Hippo\Grids\Filter;


class NotEqual extends AbstractFilter
{

    public function condition($inputs)
    {
        $value = array_get($inputs, $this->column);

        if (!isset($value)) {
            return;
        }

        $this->value = $value;

        return $this->buildCondition($this->column, '!=', $this->value);
    }
}