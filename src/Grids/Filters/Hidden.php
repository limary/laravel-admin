<?php
namespace Sinta\Hippo\Grids\Filter;


class Hidden extends AbstractFilter
{
    protected $name;

    protected $value;


    public function __construct($name, $value)
    {
        $this->name = $name;

        $this->value = $value;
    }

    public function condition($inputs)
    {
    }

    public function render()
    {
        return "<input type='hidden' name='$this->name' value='$this->value'>";
    }

}