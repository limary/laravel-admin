<?php
namespace Sinta\Laravel\Admin\Grids\Filters;

use Sinta\Laravel\Admin\Grids\Filter;

abstract class AbstractFilter
{
    protected $id;

    protected $label;

    protected $value;

    protected $defaultValue;

    protected $column;


    protected $presenter;

    protected $query = 'where';


    protected $parent;


    protected $view = 'widgets::filter.where';


    protected $manager;


    public function __construct(ManagerInterface $manager,$column, $label = '')
    {
        $this->column = $column;
        $this->label = $this->formatLabel($label);
        $this->id = $this->formatId($column);
        $this->manager = $manager;

        $this->setupDefaultPresenter();
    }




    protected function setupDefaultPresenter()
    {
        $this->setPresenter(new Presenter\Text($this->label,$this->manager));
    }


    protected function formatLabel($label)
    {
        $label = $label ?: ucfirst($this->column);

        return str_replace(['.', '_'], ' ', $label);
    }


    protected function formatName($column)
    {
        $columns = explode('.', $column);

        if (count($columns) == 1) {
            return $columns[0];
        }

        $name = array_shift($columns);
        foreach ($columns as $column) {
            $name .= "[$column]";
        }

        return $name;
    }

    protected function formatId($columns)
    {
        return str_replace('.', '_', $columns);
    }


    public function setParent(Filter $filter)
    {
        $this->parent = $filter;
    }

    public function siblings($index = null)
    {
        if (!is_null($index)) {
            return array_get($this->parent->filters(), $index);
        }

        return $this->parent->filters();
    }


    public function previous($step = 1)
    {
        return $this->siblings(
            array_search($this, $this->parent->filters()) - $step
        );
    }

    public function next($step = 1)
    {
        return $this->siblings(
            array_search($this, $this->parent->filters()) + $step
        );
    }

    public function condition($inputs)
    {
        $value = array_get($inputs, $this->column);

        if (!isset($value)) {
            return;
        }

        $this->value = $value;

        return $this->buildCondition($this->column, $this->value);
    }


    public function select($options = [])
    {
        return $this->setPresenter(new Presenter\Select($options,$this->manager));
    }


    public function multipleSelect($options = [])
    {
        return $this->setPresenter(new Presenter\MultipleSelect($options,$this->manager));
    }


    public function radio($options = [])
    {
        return $this->setPresenter(new Presenter\Radio($options,$this->manager));
    }


    public function checkbox($options = [])
    {
        return $this->setPresenter(new Presenter\Checkbox($options,$this->grid));
    }


    public function datetime($options = [])
    {
        return $this->setPresenter(new Presenter\DateTime($options,$this->manager));
    }


    public function date()
    {
        return $this->datetime(['format' => 'YYYY-MM-DD']);
    }


    public function time()
    {
        return $this->datetime(['format' => 'HH:mm:ss']);
    }


    public function day()
    {
        return $this->datetime(['format' => 'DD']);
    }

    public function month()
    {
        return $this->datetime(['format' => 'MM']);
    }


    public function year()
    {
        return $this->datetime(['format' => 'YYYY']);
    }


    protected function setPresenter(Presenter\Presenter $presenter)
    {
        $presenter->setParent($this);

        return $this->presenter = $presenter;
    }


    protected function presenter()
    {
        return $this->presenter;
    }


    public function default($default = null)
    {
        if ($default) {
            $this->defaultValue = $default;
        }

        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getColumn()
    {
        return $this->column;
    }


    public function getValue()
    {
        return $this->value;
    }



    protected function buildCondition()
    {
        $column = explode('.', $this->column);

        if (count($column) == 1) {
            return [$this->query => func_get_args()];
        }

        return $this->buildRelationQuery(...func_get_args());
    }


    protected function buildRelationQuery()
    {
        $args = func_get_args();

        list($relation, $args[0]) = explode('.', $this->column);

        return ['whereHas' => [$relation, function ($relation) use ($args) {
            call_user_func_array([$relation, $this->query], $args);
        }]];
    }

    protected function variables()
    {
        return array_merge([
            'id'        => $this->id,
            'name'      => $this->formatName($this->column),
            'label'     => $this->label,
            'value'     => $this->value ?: $this->defaultValue,
            'presenter' => $this->presenter(),
        ], $this->presenter()->variables());
    }

    public function render()
    {
        return view($this->view, $this->variables());
    }


    public function __call($method, $params)
    {
        if (method_exists($this->presenter, $method)) {
            return $this->presenter()->{$method}(...$params);
        }

        throw new \Exception('Method "'.$method.'" not exists.');
    }

    public function __toString()
    {
        return $this->render();
    }
}