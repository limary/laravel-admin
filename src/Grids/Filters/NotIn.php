<?php

namespace Sinta\Hippo\Grids\Filter;


class NotIn extends In
{
    protected $query = 'whereNotIn';
}