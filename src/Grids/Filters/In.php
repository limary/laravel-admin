<?php

namespace Sinta\Hippo\Grids\Filter;


class In extends AbstractFilter
{
    protected $query = 'whereIn';

    public function condition($inputs)
    {
        $value = array_get($inputs, $this->column);

        if (is_null($value)) {
            return;
        }

        $this->value = (array) $value;

        return $this->buildCondition($this->column, $this->value);
    }
}