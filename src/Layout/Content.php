<?php
namespace Sinta\Laravel\Admin\Layout;

use Closure;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Support\MessageBag;

/**
 * 内容
 *
 * Class Content
 * @package Sinta\Laravel\Admin\Layout
 */
class Content implements Renderable
{

    protected $header = '';

    protected $description = '';

    protected $rows = [];

    protected $breadcrumb = [];


    /**
     * 内容实例
     *
     * Content constructor.
     * @param Closure|null $callback
     * @param string $applicationType
     */
    public function __construct(Closure $callback = null)
    {
        if ($callback instanceof Closure) {
            $callback($this);
        }
    }

    public function header($header = '')
    {
        $this->header = $header;

        return $this;
    }

    public function description($description = '')
    {
        $this->description = $description;

        return $this;
    }


    public function breadcrumb(...$breadcrumb)
    {
        $this->validateBreadcrumb($breadcrumb);
        $this->breadcrumb = (array) $breadcrumb;
        return $this;
    }


    protected function validateBreadcrumb(array $breadcrumb)
    {
        foreach ($breadcrumb as $item) {
            if (!is_array($item) || !array_has($item, 'text')) {
                throw new  \Exception('Breadcrumb format error!');
            }
        }
        return true;
    }

    /**
     * Body内容部分
     *
     * @param $content
     * @return Content
     */
    public function body($content)
    {
        return $this->row($content);
    }

    /**
     * 行
     *
     * @param $content
     * @return $this
     */
    public function row($content)
    {
        if ($content instanceof Closure) {
            $row = new Row();
            call_user_func($content, $row);
            $this->addRow($row);
        } else {
            $this->addRow(new Row($content));
        }

        return $this;
    }


    protected function addRow(Row $row)
    {
        $this->rows[] = $row;
    }

    /**
     * 内容创建
     *
     * @return string
     */
    public function build()
    {
        ob_start();
        foreach ($this->rows as $row) {
            $row->build();
        }
        $contents = ob_get_contents();
        ob_end_clean();

        return $contents;
    }


    public function withError($title = '', $message = '')
    {
        $error = new MessageBag(compact('title', 'message'));

        session()->flash('error', $error);

        return $this;
    }

    /**
     * 渲染
     *
     * @return string
     */
    public function render()
    {
        $items = [
            'header'      => $this->header,
            'description' => $this->description,
            'breadcrumb'  => $this->breadcrumb,
            'content'     => $this->build(),
        ];

        return view('admin::content', $items)->render();
    }


    public function __toString()
    {
        return $this->render();
    }
}