<?php
namespace Sinta\Laravel\Admin\Layout;


interface Buildable
{
    public function build();
}