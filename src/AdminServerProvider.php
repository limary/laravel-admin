<?php
namespace Sinta\Laravel\Admin;

use Illuminate\Support\ServiceProvider;


/**
 * 服务入口
 *
 * Class AdminServerProvider
 * @package Sinta\Laravel\Admin
 */
class AdminServerProvider  extends ServiceProvider
{
    /**
     * 提供的命令
     *
     * @var array
     */
    protected $commands = [
        'Sinta\Laravel\Admin\Console\MakeCommand',
        'Sinta\Laravel\Admin\Console\MenuCommand',
        'Sinta\Laravel\Admin\Console\InstallCommand',
        'Sinta\Laravel\Admin\Console\UninstallCommand',
        'Sinta\Laravel\Admin\Console\ImportCommand',
    ];

    /**
     * 提供的路由中间件
     *
     * @var array
     */
    protected $routeMiddleware = [
        'admin.auth'        => \Sinta\Laravel\Admin\Middleware\Authenticate::class,
        'admin.pjax'        => \Sinta\Laravel\Admin\Middleware\Pjax::class,
        'admin.log'         => \Sinta\Laravel\Admin\Middleware\LogOperation::class,
        'admin.permission'  => \Sinta\Laravel\Admin\Middleware\Permission::class,
        'admin.bootstrap'   => \Sinta\Laravel\Admin\Middleware\Bootstrap::class,
    ];

    /**
     * 中间件组
     *
     * @var array
     */
    protected $middlewareGroups = [
        'admin' => [
            'admin.auth',
            'admin.pjax',
            'admin.log',
            'admin.bootstrap',
            'admin.permission',
        ],
    ];

    /**
     * 启动服务
     */
    public function boot()
    {
        //加载视图
        $this->loadViewsFrom(__DIR__.'/../resources/views','admin');

        if(file_exists($routes = admin_path('routes.php'))){
            $this->loadRoutesFrom($routes);
        }

        if($this->app->runningInConsole()){
            $this->publishes([__DIR__.'/../config' => config_path()], 'laravel-admin-config');
            $this->publishes([__DIR__.'/../resources/lang' => resource_path('lang')], 'laravel-admin-lang');
            $this->publishes([__DIR__.'/../database/migrations' => database_path('migrations')], 'laravel-admin-migrations');
            $this->publishes([__DIR__.'/../resources/assets' => public_path('vendor/laravel-admin')], 'laravel-admin-assets');
        }
    }

    /**
     * 注册服务
     */
    public function register()
    {
        $this->loadAdminAuthConfig();
        $this->registerRouteMiddleware();
        $this->commands($this->commands);
    }

    /**
     * 加载auth设置
     */
    protected function loadAdminAuthConfig()
    {
        config(array_dot(config('admin.auth',[]),'auth.'));
    }

    /**
     * 注册路由中间件
     */
    protected function registerRouteMiddleware()
    {
        foreach ($this->routeMiddleware as $key => $middleware) {
            app('router')->aliasMiddleware($key, $middleware);
        }

        foreach ($this->middlewareGroups as $key => $middleware) {
            app('router')->middlewareGroup($key, $middleware);
        }
    }

}